package logic

import (
	"context"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

func Ban(
	ctx context.Context,
	opts dto.BanOpts,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo) BLError {

	if err := opts.Validate(); err != nil {
		return NewValidationErr(err.Error(), map[string]any{})
	}

	uid, blerr := getUserIdByToken(ctx, opts.Token, sr)
	if blerr != nil {
		return blerr
	}
	reqUID, blerr := uuidOrUsername(ctx, opts.UIDorUsername, ur)
	if blerr != nil {
		return blerr
	}

	err := ur.BanUser(ctx, reqUID, uid)
	if err != nil {
		if err == entities.ErrBanYourself {
			return NewErr(err.Error(), ErrOpts{
				Log:      false,
				States:   map[string]any{"in": ERR_IN_REPOSITORY, "uid": uid},
				HTTPCode: http.StatusNotFound,
			})
		}
		if err == entities.ErrEntityNotFound {
			return NewErr(ErrEntityNotFound, ErrOpts{
				Log:      false,
				States:   map[string]any{"in": ERR_IN_REPOSITORY},
				HTTPResp: "User isn't exist, so you cannot ban him",
				HTTPCode: http.StatusNotFound,
			})
		}
		if err == entities.ErrEntityExists {
			return NewErr("User banned", ErrOpts{
				Log:      false,
				States:   map[string]any{"in": ERR_IN_REPOSITORY},
				HTTPCode: http.StatusNotFound,
			})
		}
		return NewRepositoryErr(err.Error(),
			"Failed to ban user due the unexpected error", map[string]any{})
	}
	return nil
}

func Unban(
	ctx context.Context,
	opts dto.BanOpts,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo) BLError {

	if err := opts.Validate(); err != nil {
		return NewValidationErr(err.Error(), map[string]any{})
	}

	uid, blerr := getUserIdByToken(ctx, opts.Token, sr)
	if blerr != nil {
		return blerr
	}
	reqUID, blerr := uuidOrUsername(ctx, opts.UIDorUsername, ur)
	if blerr != nil {
		return blerr
	}

	err := ur.UnbanUser(ctx, reqUID, uid)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return NewErr(ErrEntityNotFound, ErrOpts{
				Log:      false,
				States:   map[string]any{"in": ERR_IN_REPOSITORY},
				HTTPResp: "User isn't banned",
				HTTPCode: http.StatusNotFound,
			})
		}
		return NewRepositoryErr(err.Error(),
			"Failed to unban user due the unexpected error", map[string]any{})
	}
	return nil
}

func GetBanned(
	ctx context.Context,
	user dto.CreateUserOpts,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo) BLError {

	return nil
}
