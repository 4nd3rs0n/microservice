package users_test

import (
	"context"
	"fmt"
	"os"
	"testing"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/app"
)

var CFG_API_Port = 8000
var CFG_API_Prefix = "/"
var CFG_DB_Migrations = "/sql"
var appInstance app.App

func TestMain(m *testing.M) {
	var err error
	ctx := context.Background()

	appInstance, err = app.InitApp(ctx, app.Config{
		StartHTTP:     true,
		HTTPport:      CFG_API_Port,
		HttpApiPrefix: CFG_API_Prefix,

		StartGRPC: false,

		PgURL: fmt.Sprintf(
			"postgresql://%s:%s@%s:%s/%s",
			os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD"),
			os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"),
			os.Getenv("POSTGRES_DB")),
		MigrationPath: "sql",

		RedisURL: fmt.Sprintf("%s:%s",
			os.Getenv("REDIS_HOST"),
			os.Getenv("REDIS_PORT")),
		RedisPassword: os.Getenv("REDIS_PASSWORD"),
		RedisBD:       0,

		Stage: os.Getenv("STAGE"),
	})
	if err != nil {
		panic(err)
	}

	go appInstance.Run(ctx)
	defer appInstance.Close(ctx)

	m.Run()
}
