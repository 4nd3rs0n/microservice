package validate

const (
	Name      = `^.*$`
	Login     = `^[a-zA-Z0-9][a-zA-Z0-9_]*$`
	DeviceInf = `^.{,150}$`
	IpAddr    = `^(?:(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|(?:[0-9]{1,3}\.){3}[0-9]{1,3})$`
)
