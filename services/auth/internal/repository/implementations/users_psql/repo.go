package users

import (
	"context"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/username"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type PostgresInterface struct {
	pool *pgxpool.Pool
}

func NewPostrgerInterface(pool *pgxpool.Pool) *PostgresInterface {
	return &PostgresInterface{
		pool: pool,
	}
}

// Accept the User ID and return the user or an error
func (p *PostgresInterface) GetUserByID(ctx context.Context, id uuid.UUID) (entities.User, error) {
	user := entities.User{}
	var username_base string
	var username_id int

	err := p.pool.QueryRow(ctx, ""+
		"SELECT u.id, "+
		"c.login, ud.displayed_name, "+
		"ud.created_at, ud.username, ud.username_id "+
		"FROM public.users u "+
		"JOIN public.creds c ON u.id = c.userId "+
		"JOIN user_data ud ON u.id = ud.userId "+
		"WHERE u.id = $1;", id).Scan(
		&user.ID, &user.Login, &user.Name, &user.CreatedAt,
		&username_base, &username_id)

	uname, err := username.NewUsername(username_base, username_id)
	if err != nil {
		return user, err
	}
	user.Username = uname
	return user, err
}

// Accept the login and return the user or an error
func (p *PostgresInterface) GetUserIdByLogin(ctx context.Context, login string) (uuid.UUID, error) {
	var uID uuid.UUID
	err := p.pool.QueryRow(ctx, ""+
		"SELECT userId FROM public.creds "+
		"WHERE c.login = $1;", login).Scan(&uID)
	return uID, err
}

func (p *PostgresInterface) IsLoginTaken(ctx context.Context, login string) (bool, error) {
	var taken bool
	err := p.pool.QueryRow(ctx, ""+
		"SELECT EXISTS(SELECT 1 FROM public.creds WHERE login = $1);",
		login).Scan(&taken)
	return taken, err
}

func (p *PostgresInterface) VerifyLoginCreds(ctx context.Context, creds entities.Creds) (uuid.UUID, bool, error) {
	var uid uuid.UUID
	var actualPasswordHash, salt []byte

	err := p.pool.QueryRow(ctx, ""+
		"SELECT userId, password_hash, salt "+
		"FROM public.creds "+
		"WHERE login = $1;",
		creds.Login).Scan(&uid, &actualPasswordHash, &salt)
	if err != nil {
		if err == pgx.ErrNoRows {
			return uuid.Nil, false, nil
		}
		return uuid.Nil, false, err
	}

	valid := verifyPassword(creds.Password, salt, actualPasswordHash)
	return uid, valid, err
}
func (p *PostgresInterface) VerifyIdAndPassword(ctx context.Context, uid uuid.UUID, password string) (bool, error) {
	var actualPasswordHash, salt []byte

	err := p.pool.QueryRow(ctx, ""+
		"SELECT password_hash, salt "+
		"FROM public.creds "+
		"WHERE userId = $1;",
		uid).Scan(&actualPasswordHash, &salt)
	if err != nil {
		if err == pgx.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	valid := verifyPassword(password, salt, actualPasswordHash)
	return valid, nil
}

func (p *PostgresInterface) GetCountOfUsers(ctx context.Context) (int, error) {
	var count int
	err := p.pool.QueryRow(ctx, "SELECT COUNT(*) FROM public.users;").Scan(&count)
	if err != nil {
		return 0, err
	}
	return count, err
}

func (p *PostgresInterface) ChangeLogin(ctx context.Context, uID uuid.UUID, newLogin string) error {
	_, err := p.pool.Exec(ctx, ""+
		"UPDATE creds "+
		"SET login = $1 "+
		"WHERE userId = $2;", newLogin, uID)
	return err
}
func (p *PostgresInterface) ChangePassword(ctx context.Context, uID uuid.UUID, newPassowrd string) error {
	passwordHash, salt, err := hashPassword(newPassowrd)
	if err != nil {
		return err
	}
	_, err = p.pool.Exec(ctx, ""+
		"UPDATE creds "+
		"SET password_hash = $1, "+
		"salt = $2 "+
		"WHERE userId = $3;", passwordHash, salt, uID)
	return err
}
func (p *PostgresInterface) ChangeDisplayedName(ctx context.Context, uID uuid.UUID, newName string) error {
	_, err := p.pool.Exec(ctx, ""+
		"UPDATE user_data "+
		"SET displayed_name = $1 "+
		"WHERE userId = $2;", newName, uID)
	return err
}
