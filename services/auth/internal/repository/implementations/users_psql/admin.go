package users

import (
	"context"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

func (p *PostgresInterface) MakeAdmin(ctx context.Context, uID uuid.UUID) error {
	_, err := p.pool.Exec(ctx, ""+
		"INSERT INTO public.admins (userId) VALUES ($1)", uID)
	if err != nil && strings.Contains(err.Error(), "duplicate key") {
		return entities.ErrEntityExists
	}
	return err
}

func (p *PostgresInterface) IsAdmin(ctx context.Context, uID uuid.UUID) (bool, error) {
	var admin bool
	err := p.pool.QueryRow(ctx, ""+
		"SELECT EXISTS(SELECT 1 FROM admins WHERE userId = $1);", uID).Scan(&admin)
	return admin, err
}

func (p *PostgresInterface) GetAllAdmins(ctx context.Context) ([]entities.UserWithoutLogin, error) {
	var resp []entities.UserWithoutLogin

	err := p.pool.QueryRow(ctx, ""+
		"SELECT a.userId, ud.displayed_name "+
		"FROM admins a "+
		"JOIN user_data ud ON a.userId = ud.userId;").Scan(&resp)
	return resp, err
}
