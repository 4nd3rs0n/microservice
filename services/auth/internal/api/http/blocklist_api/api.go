package blocklist_api

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
	"golang.org/x/exp/slog"
)

type BlocklistAPI struct {
	ur     interfaces.UsersRepo
	sr     interfaces.SessionsRepo
	logger *slog.Logger
}

func NewBlocklistAPI() BlocklistAPI {
	return BlocklistAPI{}
}

func (bapi *BlocklistAPI) API(ctx context.Context, r *chi.Mux) {
	r.Route("/ban", func(r chi.Router) {
		r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			switch r.Method {
			case http.MethodOptions:
				w.WriteHeader(http.StatusOK)
			case http.MethodPost:
				// bapi.ban(ctx, w, r)
			case http.MethodGet:
				// bapi.getBanned(ctx, w, r)
			case http.MethodDelete:
				// bapi.unban(ctx, w, r)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})
	})
}
