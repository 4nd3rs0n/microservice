package sessions_api

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
	"golang.org/x/exp/slog"
)

type SessionsAPI struct {
	ur     interfaces.UsersRepo
	sr     interfaces.SessionsRepo
	logger *slog.Logger
}

func NewSessionsAPI(
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo,
	logger *slog.Logger) SessionsAPI {

	return SessionsAPI{
		ur:     ur,
		sr:     sr,
		logger: logger,
	}
}

func (sa *SessionsAPI) API(ctx context.Context, r *chi.Mux) {
	r.Route("/sessions", func(r chi.Router) {
		r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			if r.Method == http.MethodOptions {
				w.WriteHeader(http.StatusOK)
				return
			}
			if r.Method != http.MethodGet {
				w.WriteHeader(http.StatusMethodNotAllowed)
				return
			}

			usrTkn, next, err := middleware.TokenPrehandler(w, r)
			if err != nil {
				sa.logger.Error("Failed to handle",
					slog.String("method", r.Method),
					slog.String("URL", r.URL.Scheme),
					slog.String("error", err.Error()))
				return
			}
			if !next {
				return
			}

			err = sa.GetAllSessions(ctx, w, r, usrTkn)
			if err != nil {
				sa.logger.Error("Failed to handle",
					slog.String("method", r.Method),
					slog.String("URL", r.URL.Scheme),
					slog.String("error", err.Error()))
				return
			}
		})

		r.HandleFunc("/{tknOrID}", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			switch r.Method {
			case http.MethodOptions:
				w.WriteHeader(http.StatusOK)
			case http.MethodGet:
				sa.GetSession(ctx, w, r)
			case http.MethodDelete:
				sa.KillSession(ctx, w, r)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})
	})
}
