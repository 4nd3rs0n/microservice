package server

import (
	"context"

	login "gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/login_api"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/sessions_api"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/users_api"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
	"golang.org/x/exp/slog"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type HttpApiOpts struct {
	ApiPrefix string
	Users     interfaces.UsersRepo
	Sessions  interfaces.SessionsRepo
	Logger    *slog.Logger

	LogAllRequests bool
	LogInternalErr bool
}

func NewApi(ctx context.Context, opts HttpApiOpts) (r *chi.Mux, err error) {
	r = chi.NewRouter()

	r.Use(middleware.Logger)

	uApi := users_api.NewUsersAPI(opts.Users, opts.Sessions, opts.Logger)
	sApi := sessions_api.NewSessionsAPI(opts.Users, opts.Sessions, opts.Logger)
	lApi := login.NewLoginAPI(opts.Users, opts.Sessions, opts.Logger)

	r.Route(opts.ApiPrefix, func(w chi.Router) {
		uApi.API(ctx, r)
		sApi.API(ctx, r)
		lApi.API(ctx, r)
	})

	return
}
