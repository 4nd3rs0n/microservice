package logic

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

func IsAdmin(
	ctx context.Context,
	uid uuid.UUID,
	ur interfaces.UsersRepo) (bool, BLError) {

	is, err := ur.IsAdmin(ctx, uid)
	if err != nil {
		return false, NewErr(err.Error(), ErrOpts{})
	}
	return is, nil
}
