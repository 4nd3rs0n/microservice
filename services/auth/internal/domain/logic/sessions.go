package logic

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

func KillSession(
	ctx context.Context,
	opts dto.KillSessionOpts,
	sr interfaces.SessionsRepo) BLError {

	if len(opts.Token) != entities.TOKEN_LENGTH {
		return NewValidationErr(ErrTokenLength, map[string]any{})
	}

	uID, blerr := getUserIdByToken(ctx, opts.Token, sr)
	if blerr != nil {
		return blerr
	}
	session, err := sr.GetSessionInfoByID(ctx, opts.SID)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return NewErr(ErrEntityNotFound, ErrOpts{
				Log:      false,
				States:   map[string]any{},
				HTTPResp: "Failed",
				HTTPCode: http.StatusUnauthorized,
			})
		}
		return NewRepositoryErr(err.Error(),
			"Failed to get uid by session due the unexpected error",
			map[string]any{})
	}

	if session.UID == uID {
		return NewErr(ErrPermissionDenied, ErrOpts{
			Log: false,
			States: map[string]any{
				"in":          ERR_IN_PERMISSION_CHECK,
				"uid":         uID,
				"session-uid": session.UID,
			},
			HTTPCode: http.StatusForbidden,
		})
	}

	err = sr.KillSession(ctx, opts.SID)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return NewErr("Session wasn't found", ErrOpts{
				Log:      false,
				States:   map[string]any{},
				HTTPCode: http.StatusNotFound,
			})
		}
		return NewRepositoryErr(err.Error(),
			"", map[string]any{})
	}
	return nil
}

func GetAllMySessions(
	ctx context.Context,
	token string,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo) (dto.GetAllMySessionsResp, BLError) {

	resp := dto.GetAllMySessionsResp{}

	if len(token) != entities.TOKEN_LENGTH {
		return resp, NewValidationErr(ErrTokenLength, map[string]any{})
	}

	uID, blerr := getUserIdByToken(ctx, token, sr)
	if blerr != nil {
		return resp, blerr
	}
	sessions, err := sr.GetAllSessions(ctx, uID)
	if err != nil {
		return resp, NewRepositoryErr(err.Error(),
			"", map[string]any{})
	}
	resp.Sessions = sessions

	return resp, nil
}

// For admins only
func GetAllSessions(
	ctx context.Context,
	opts dto.GetAllSessionsOpts,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo) {

}

func GetSession(
	ctx context.Context,
	opts dto.GetSessionOpts,
	sr interfaces.SessionsRepo,
	ur interfaces.UsersRepo) (dto.GetSessionResp, BLError) {

	var resp dto.GetSessionResp
	var session entities.Session

	currentSID, currentUID, err := sr.GetSIdAndUIdByToken(ctx, opts.Token)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return resp, NewErr(ErrTokenInvalid, ErrOpts{
				Log:      false,
				States:   map[string]any{"in": ERR_IN_PERMISSION_CHECK},
				HTTPCode: http.StatusUnauthorized,
			})
		}
		return resp, NewRepositoryErr(err.Error(),
			"Failed to get your current session due the unexpected error",
			map[string]any{})
	}

	reqSID, err := uuid.Parse(opts.SidOrTkn)
	if err != nil {
		token := opts.SidOrTkn
		if len(token) != entities.TOKEN_LENGTH {
			return resp, NewValidationErr("Session ID or token is invalid",
				map[string]any{})
		}
		session, err = sr.GetSessionInfoByToken(ctx, token)
	} else {
		session, err = sr.GetSessionInfoByID(ctx, reqSID)
	}

	if err != nil {
		if err == entities.ErrEntityNotFound {
			return resp, NewErr(ErrEntityNotFound, ErrOpts{
				Log:      false,
				States:   map[string]any{"in": ERR_IN_PERMISSION_CHECK},
				HTTPResp: "Session wasn't found",
				HTTPCode: http.StatusUnauthorized,
			})
		}
		return resp, NewRepositoryErr(err.Error(),
			"Failed to get session information", map[string]any{})
	}

	sameUser := session.UID == currentUID
	// TODO: Admin table
	admin, err := ur.IsAdmin(ctx, currentUID)
	if err != nil {
		return resp, NewRepositoryErr(err.Error(),
			"Failed to check your admin rights",
			map[string]any{"in": ERR_IN_REPOSITORY, "uid": currentUID})
	}
	if !sameUser && !admin {
		return resp, NewErr(ErrPermissionDenied, ErrOpts{
			Log:      false,
			States:   map[string]any{"in": ERR_IN_PERMISSION_CHECK},
			HTTPCode: http.StatusForbidden,
		})
	}

	resp.UID = session.UID
	resp.SID = session.SID
	resp.CreatedAt = session.CreatedAt
	resp.DeviceInf = session.DeviceInf
	resp.CreatorIP = session.CreatorIP
	resp.Current = session.SID == currentSID

	return resp, nil
}
