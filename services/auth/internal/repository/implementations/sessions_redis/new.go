package sessions

import (
	"context"
	"crypto"
	cryptoRand "crypto/rand"
	"encoding/hex"
	"time"

	"github.com/google/uuid"
	redis "github.com/redis/go-redis/v9"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

// Creates new session for the user, returns the token to use this session or an error
func (ri *RedisInterface) NewSession(ctx context.Context, usrId uuid.UUID, deviceInf string) (string, error) {
	const nilToken string = ""
	// Generate random bytes
	tokenBytes := make([]byte, entities.TOKEN_BYTES_LENGTH, entities.TOKEN_BYTES_LENGTH)
	_, err := cryptoRand.Read(tokenBytes)
	if err != nil {
		return nilToken, err
	}
	// By the security reasons we are storing in redis
	// hashes of the tokens
	// But I don't want to use bcrypt or argon2
	// due the perfoemance reasons
	// sha256 is secure enougth
	sha := crypto.SHA256.New()
	sha.Write(tokenBytes)
	hashTokenBytes := sha.Sum(nil)
	hashTokenStr := hex.EncodeToString(hashTokenBytes)

	var sessionID uuid.UUID = uuid.New()
	sessionData := map[string]interface{}{
		// Session Token hash.
		// Needed in the session data to be able to
		// remove token from chache when killing session
		"sth": hashTokenStr,
		// User Id
		"uid": usrId.String(),
		// Created at
		"cat": time.Now().UTC().Unix(),
		// Device information
		"dinf": deviceInf,
		// TODO: use the real ip
		// Created from the IP
		"cfip": "127.0.0.1",
	}
	// Data associated with token hash
	dawth := map[string]interface{}{
		// Session ID. Can be used to get detail inforamtion about the
		"sid": sessionID.String(),
		"uid": usrId.String(),
	}

	_, err = (*ri.Client).Pipelined(ctx, func(pipe redis.Pipeliner) error {
		var err error
		// Map session data with session ID
		// Session ID allows to manipulate the session
		err = pipe.HSet(ctx, "sessions.id."+sessionID.String(), sessionData).Err()
		if err != nil {
			return err
		}
		// Map session token hash with the session ID and userID
		// Used to grant or deny access latter based on Authorization header
		err = pipe.HSet(ctx, "sessions.token."+hashTokenStr, dawth).Err()
		if err != nil {
			return err
		}
		// Map user's ID with session token hash
		// Used to get all the session that are related to the user
		err = pipe.SAdd(ctx, "users."+usrId.String()+".sessions", sessionID.String()).Err()
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nilToken, err
	}
	token := hex.EncodeToString(tokenBytes)
	return token, nil
}
