package users_api

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/logic"
)

type SetUsernameBody struct {
	Username string `json:"username"`
}

// Handles PATCH request when
func (uapi *UsersAPI) SetUsernameH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	var body SetUsernameBody
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	token, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	errx := logic.SetUsername(ctx, dto.SetUsernameOpts{
		Token:        token,
		UsernameBase: body.Username,
	}, uapi.ur, uapi.sr)

	if errx != nil {
		logic.HTTPHandleError(uapi.logger, w, r, errx)
		return
	}

	w.WriteHeader(http.StatusOK)
}
