package dto

type LoginOpts struct {
	Password   string
	Login      string
	DeviceInfo string
	IpAddr     string
}
type LoginResponse struct {
	Token string
}
