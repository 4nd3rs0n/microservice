module gitlab.com/4nd3rs0n/microservice/services/auth

go 1.20

require (
	github.com/go-chi/chi/v5 v5.0.8
	github.com/go-redis/redismock/v9 v9.0.3
	github.com/google/uuid v1.3.0
	github.com/jackc/pgx/v5 v5.3.1
	github.com/redis/go-redis/v9 v9.0.4
	gitlab.com/4nd3rs0n/errorsx v0.0.0-20230521125317-b4d0f1348e63
	golang.org/x/exp v0.0.0-20230519143937-03e91628a987
	google.golang.org/grpc v1.55.0
	google.golang.org/protobuf v1.30.0
)

require (
	github.com/lib/pq v1.10.9 // indirect
	github.com/stretchr/testify v1.8.3 // indirect
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/golang-migrate/migrate/v4 v4.15.2 // direct
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.0 // indirect
	go.uber.org/atomic v1.11.0 // indirect
	golang.org/x/crypto v0.9.0 // direct
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sync v0.2.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	google.golang.org/genproto v0.0.0-20230410155749-daa745c078e1 // indirect
)
