BEGIN;

CREATE TABLE IF NOT EXISTS users (
  -- Table "users" stores only IDs.
  -- If there is need to store something
  -- else with user, you have to create
  -- another migration. And hopefully
  -- without changing this table, but
  -- creating another with reference to
  -- the user's ID
  id  UUID PRIMARY KEY DEFAULT gen_random_uuid() UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS creds (
  -- All credentials for the authorisation 
  -- are stored here.
  userId        UUID REFERENCES users(id) ON DELETE CASCADE UNIQUE NOT NULL,
  -- Login is gotta be used only as account credentials. 
  -- It's not gonna go public
  login         TEXT NOT NULL UNIQUE,
  password_hash BYTEA NOT NULL,
  salt          BYTEA NOT NULL
);

CREATE TABLE IF NOT EXISTS user_data (
  userId         UUID REFERENCES users(id) ON DELETE CASCADE UNIQUE NOT NULL,
  
  -- @uname#uname_id is an unique human-readable user indificator
  -- It's used to make easier to users to share their account
  -- and stay login private at the same time.
  username       TEXT,
  username_id    INT,
  CONSTRAINT username_unique UNIQUE (username, username_id),
  
  displayed_name TEXT NOT NULL,
  created_at     TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

COMMIT;
