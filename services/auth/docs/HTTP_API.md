# HTTP API 

- `apiprfx` -- API prefix can be used for API versioning. Default API prefix is `/`
- `Authorization Header` -- `Authorization: Session <token>`

## Users
<br/>

### POST {apiprfx}/users
**JSON body:**  
- `name` string. 1-49 symbols. Some unicode chars are blocked
- `login` string. 3-40 symbols. Only a-z 0-9 and _
- `password` string. Min 6 symbols

Creates a new user and returns Session Token that can be used later for the authorization<br/><br/>

### GET {apiprfx}/me
**Requiers Authorization Header**  
Returns your account info<br/><br/>

### GET {apiprfx}/users/{uuid-or-login}
Returns someone's other information<br/><br/>

### DELETE {apiprfx}/me
**Requiers Authorization Header**  
Deletes your account<br/><br/>

### DELETE {apiprfx}/users/{uuid-or-login}
**Requiers Authorization Header**  
Admin edpoint. Deletes someone's other account<br/><br/>

### GET {apiprfx}/users
**Requiers Authorization Header**<br/><br/>

### POST {apiprfx}/login
**JSON body:**  
- `login` string. 3-40 symbols. Only a-z 0-9 and _
- `password` string. Min 6 symbols

Returns Session Token that can be used later for the authorization<br/><br/>

### GET {apiprfx}/logout
**Requiers Authorization Header**  
Kills your current session<br/><br/>

## Sessions


## Blocklist
Banned by you users cannot see your account. They'll see the same if you delete it<br/><br/>

### POST {apiprfx}/ban/
**Requiers Authorization Header**  
**JSON body:**  
- `user` uuid or login  

Ban user<br/><br/>

### DELETE {apiprfx}/ban/{uuid-or-login}
**Requiers Authorization Header**  
Unblocks user<br/><br/>

### GET {apiprfx}/ban
**Requiers Authorization Header**  
Return all users that are banned by you<br/><br/>

## Admins
Admins can remove other users, can see everyone, there is no way to block admin<br/><br/>

### POST {apiprfx}/admins/
**Requiers Authorization Header**  
**JSON body:**  
- `user` uuid or login 

Maybe later will add some other options in the request body, like time limit for admins or premissions  
Endpoint for admins to make someone other admin as well<br/><br/>

### GET {apiprfx}/admins/
**Requiers Authorization Header**<br/><br/>  

This service has no endpoint to delete an admin because there is no admins priority, so any admin whould be able to remove any other admin. If you need to remove an admin -- you'll need to make a request to DB directly. 