package app

import (
	"context"
	"time"

	"github.com/redis/go-redis/v9"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/dbtools"
	"golang.org/x/exp/slog"
)

func (a *App) initRedis(ctx context.Context,
	redisURL string, redisPassword string, redisDB int) error {

	a.logger.Debug("Initializing Redis client")
	a.rc = redis.NewClient(&redis.Options{
		Addr:     redisURL,
		Password: redisPassword,
		DB:       redisDB, // use default DB
	})

	checkUpRdsOpts := dbtools.CheckUpOpts{
		Ctx:     ctx,
		Retries: 3,
		Timeout: 10 * time.Second,
		Pingable: &dbtools.PingAdptrRds{
			Conn: a.rc,
		},
		Logger: a.logger,
		DBname: "Redis",
	}

	err := dbtools.CheckUp(checkUpRdsOpts)
	if err != nil {
		a.logger.Error(err.Error())
		return err
	}

	a.logger.Debug("Initializing Repository",
		slog.String("repository", "sessions"))
	a.sdbi = interfaces.NewSessionsRepo(a.rc)

	return nil
}
