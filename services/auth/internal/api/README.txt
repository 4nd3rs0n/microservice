Here is stored everything related to the application APIs 
Basically it's a transport layer of this service

Here you'll find 
- Route declarations
- Handlers 
- Middlewares (and data validation)

API depends only on the internal/domain package
It's shouldn't use anithing from the repository