package app

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/dbtools"
	"golang.org/x/exp/slog"
)

func (a *App) initPostgres(ctx context.Context,
	pgURL string, pgMigrationsPath string) error {

	a.logger.Debug("Connecting to the PostgreSQL for setup")
	pgxConn, err := pgx.Connect(ctx, pgURL)
	if err != nil {
		a.logger.Error(err.Error())
		return err
	}

	checkUpPgxOpts := dbtools.CheckUpOpts{
		Ctx:     ctx,
		Retries: 3,
		Timeout: 10 * time.Second,
		Pingable: &dbtools.PingAdptrPgSQL{
			Conn: pgxConn,
		},
		Logger: a.logger,
		DBname: "PostgreSQL",
	}
	err = dbtools.CheckUp(checkUpPgxOpts)
	if err != nil {
		a.logger.Error(err.Error())
		return err
	}

	a.logger.Debug("Closing the setup PostgreSQL connection")
	pgxConn.Close(ctx)

	mOpts := dbtools.MigrationOpts{
		MSrc:    fmt.Sprintf("file://%s", pgMigrationsPath),
		DBurl:   pgURL,
		VerLim:  0,
		DropDev: true,
		Logger:  a.logger,
	}

	_, _, errm := dbtools.DoMigrate(mOpts)
	if errm != nil {
		errmInf := errm.Info()
		a.logger.Error(
			fmt.Sprintf("Failed to apply DB migrations: %s", errm.Error()),
			slog.String("where", errmInf.Where),
		)
		return errm
	}

	a.logger.Debug("Initializing PostgreSQL pool")
	pgxPoolConf, err := pgxpool.ParseConfig(pgURL)
	if err != nil {
		a.logger.Error(
			fmt.Sprintf("Failed to parse pgx pool config: %s", err.Error()))
		return err
	}

	a.pgPool, err = pgxpool.NewWithConfig(ctx, pgxPoolConf)
	if err != nil {
		a.logger.Error(
			fmt.Sprintf("Failed to connect to PostgreSQL: %s\n", err.Error()))
	}

	a.logger.Debug("Initializing Repository",
		slog.String("repository", "users"))
	a.udbi = interfaces.NewUsersRepo(a.pgPool)

	return nil
}
