package sessions

import (
	"context"
	"errors"
	"strconv"

	"github.com/google/uuid"
	redis "github.com/redis/go-redis/v9"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

func (ri *RedisInterface) GetSessionIdByToken(ctx context.Context, tkn string) (uuid.UUID, error) {
	tokenHashStr, err := HashToken(tkn)
	if err != nil {
		return uuid.Nil, err
	}

	sIDstr, err := (*ri.Client).HGet(ctx, "sessions.token."+tokenHashStr, "sid").Result()
	if err != nil {
		if err == redis.Nil {
			return uuid.Nil, entities.ErrEntityNotFound
		}
		return uuid.Nil, err
	}

	sessionID, err := uuid.Parse(sIDstr)
	return sessionID, err
}

func (ri *RedisInterface) GetAllSessions(ctx context.Context, uID uuid.UUID) ([]entities.Session, error) {
	var sessions []entities.Session

	result, err := (*ri.Client).SMembers(ctx, "users."+uID.String()+".sessions").Result()

	if err != nil {
		return sessions, err
	}

	for _, sIDstr := range result {
		sID, err := uuid.Parse(sIDstr)
		if err != nil {
			return sessions, err
		}
		session, err := ri.GetSessionInfoByID(ctx, sID)
		if err != nil {
			return sessions, err
		}
		sessions = append(sessions, session)
	}

	return sessions, nil
}

func (ri *RedisInterface) GetSessionInfoByToken(ctx context.Context, token string) (entities.Session, error) {
	sID, err := ri.GetSessionIdByToken(ctx, token)
	if err != nil {
		return entities.Session{}, err
	}
	session, err := ri.GetSessionInfoByID(ctx, sID)
	return session, err
}

func (ri *RedisInterface) GetSessionInfoByID(ctx context.Context, sessionID uuid.UUID) (entities.Session, error) {
	var sInf entities.Session = entities.Session{}

	mapCmd, err := (*ri.Client).HGetAll(ctx, "sessions.id."+sessionID.String()).Result()
	if err != nil {
		if err == redis.Nil {
			return sInf, entities.ErrEntityNotFound
		}
		return sInf, err
	}

	// Checking are values exists
	uidStr, exists := mapCmd["uid"]
	if !exists {
		return sInf, errors.New("Value of uid wasn't found in this session. Probably this session doesn't exist")
	}
	createdAtStr, exists := mapCmd["cat"]
	if !exists {
		return sInf, errors.New("Value of cat wasn't found in this session.")
	}
	deviceInf, exists := mapCmd["dinf"]
	if !exists {
		return sInf, errors.New("Value of dinf wasn't found in this session.")
	}
	creatorIP, exists := mapCmd["cfip"]
	if !exists {
		return sInf, errors.New("Value of cfip wasn't found in this session.")
	}

	// Parsing values
	uid, err := uuid.Parse(uidStr)
	if err != nil {
		return sInf, err
	}
	createdAt, err := strconv.ParseInt(createdAtStr, 10, 64)
	if err != nil {
		return sInf, err
	}

	sInf = entities.Session{
		SID:       sessionID,
		UID:       uid,
		CreatedAt: createdAt,
		DeviceInf: deviceInf,
		CreatorIP: creatorIP,
	}

	return sInf, err
}

func (ri *RedisInterface) GetUserIdByToken(ctx context.Context, tkn string) (uuid.UUID, error) {
	tokenHashStr, err := HashToken(tkn)
	if err != nil {
		return uuid.Nil, err
	}

	uIDstr, err := (*ri.Client).HGet(ctx, "sessions.token."+tokenHashStr, "uid").Result()
	if err != nil {
		if err == redis.Nil {
			return uuid.Nil, entities.ErrEntityNotFound
		}
		return uuid.Nil, err
	}

	usrID, err := uuid.Parse(uIDstr)
	return usrID, err
}

func (ri *RedisInterface) GetSIdAndUIdByToken(ctx context.Context, tkn string) (uuid.UUID, uuid.UUID, error) {
	tokenHashStr, err := HashToken(tkn)
	if err != nil {
		return uuid.Nil, uuid.Nil, err
	}

	mapCmd, err := (*ri.Client).HGetAll(ctx, "sessions.token."+tokenHashStr).Result()
	if err != nil {
		if err == redis.Nil {
			return uuid.Nil, uuid.Nil, entities.ErrEntityNotFound
		}
		return uuid.Nil, uuid.Nil, err
	}
	uidStr, exists := mapCmd["uid"]
	if !exists {
		return uuid.Nil, uuid.Nil, errors.New("Value of uid wasn't found in this session. Probably this session doesn't exist")
	}
	sidStr, exists := mapCmd["sid"]
	if !exists {
		return uuid.Nil, uuid.Nil, errors.New("Value of sid wasn't found in this session. Probably this session doesn't exist")
	}

	userID, err := uuid.Parse(uidStr)
	if err != nil {
		return uuid.Nil, uuid.Nil, err
	}
	sessionID, err := uuid.Parse(sidStr)
	if err != nil {
		return uuid.Nil, uuid.Nil, err
	}
	return sessionID, userID, err
}
