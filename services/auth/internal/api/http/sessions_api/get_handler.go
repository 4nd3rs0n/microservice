package sessions_api

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/logic"
)

// Handling request to /sessions/{tknOrID}
func (sa *SessionsAPI) GetSession(
	ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	token, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	opts := dto.GetSessionOpts{
		Token:    token,
		SidOrTkn: chi.URLParam(r, "tknOrID"),
	}
	session, errx := logic.GetSession(ctx, opts, sa.sr, sa.ur)
	if errx != nil {
		logic.HTTPHandleError(sa.logger, w, r, errx)
		return
	}

	sessionJSON, err := json.Marshal(session)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to marshal response body"))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(sessionJSON)

	return
}
