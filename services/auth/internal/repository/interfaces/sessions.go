package interfaces

import (
	"context"

	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	sessions "gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/implementations/sessions_redis"
)

type SessionsRepo interface {
	NewSession(ctx context.Context, uID uuid.UUID, deviceInf string) (sToken string, err error)

	KillSession(ctx context.Context, sID uuid.UUID) error
	KillAllSessions(ctx context.Context, uID uuid.UUID) (removedItems int, err error)

	GetAllSessions(ctx context.Context, uID uuid.UUID) ([]entities.Session, error)

	GetUserIdByToken(ctx context.Context, sToken string) (uID uuid.UUID, err error)
	GetSessionIdByToken(ctx context.Context, sToken string) (ID uuid.UUID, err error)
	GetSIdAndUIdByToken(ctx context.Context, sToken string) (sID uuid.UUID, uID uuid.UUID, err error)

	GetSessionInfoByToken(ctx context.Context, sToken string) (entities.Session, error)
	GetSessionInfoByID(ctx context.Context, sID uuid.UUID) (entities.Session, error)
}

// The default interface implementation
func NewSessionsRepo(client *redis.Client) SessionsRepo {
	return &sessions.RedisInterface{
		Client: client,
	}
}
