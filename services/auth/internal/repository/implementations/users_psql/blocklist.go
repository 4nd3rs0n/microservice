package users

import (
	"context"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

// Add user to the blocklist
func (p *PostgresInterface) BanUser(ctx context.Context, bannedId uuid.UUID, bannedBy uuid.UUID) error {
	if bannedBy == bannedId {
		return entities.ErrBanYourself
	}
	_, err := p.pool.Exec(ctx, ""+
		"INSERT INTO public.blocklist (banned_id, banned_by_id) "+
		"VALUES ($1, $2);", bannedId, bannedBy)
	if err != nil && strings.Contains(err.Error(), "duplicate key") {
		return entities.ErrEntityExists
	}
	return err
}

func (p *PostgresInterface) UnbanUser(ctx context.Context, bannedId uuid.UUID, bannedBy uuid.UUID) error {
	_, err := p.pool.Exec(ctx, ""+
		"DELETE FROM public.blocklist "+
		"WHERE banned_id = $1 AND "+
		"banned_by_id = $2;", bannedId, bannedBy)
	return err
}

func (p *PostgresInterface) IsBanned(ctx context.Context, bannedId uuid.UUID, bannedById uuid.UUID) (bool, error) {
	var banned bool
	err := p.pool.QueryRow(ctx, ""+
		"SELECT EXISTS(SELECT 1 FROM public.blocklist "+
		"WHERE banned_id = $1 AND banned_by_id = $2)",
		bannedId, bannedById).Scan(&banned)
	return banned, err
}

// TODO: Scan data into the structure
func (p *PostgresInterface) GetBannedByUserID(ctx context.Context, uID uuid.UUID) ([]entities.UserWithoutLogin, error) {
	var resp []entities.UserWithoutLogin

	err := p.pool.QueryRow(ctx, ""+
		"SELECT b.banned_id, ud.displayed_name "+
		"FROM public.blocklist b "+
		"JOIN user_data ud ON b.banned_id = ud.userId "+
		"WHERE b.banned_by_id = $1;", uID).Scan(&resp)
	return resp, err
}

// TODO: Scan data into the structure
func (p *PostgresInterface) GetByWhoBannedUserID(ctx context.Context, uID uuid.UUID) error {
	var resp []entities.UserWithoutLogin

	err := p.pool.QueryRow(ctx, ""+
		"SELECT b.banned_by_id, ud.displayed_name "+
		"FROM public.blocklist b "+
		"JOIN user_data ud ON b.banned_by_id = ud.userId "+
		"WHERE b.banned_id = $1;", uID).Scan(&resp)
	return err
}
