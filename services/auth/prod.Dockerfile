FROM golang:latest

ENV STAGE="prod"
ENV CONFIG="/etc/auth-server-config.yml"

COPY ./dotfiles/config.yml /etc/auth-server-config.yml
COPY . .

RUN go mod download
RUN go build -o ./bin/auth_server ./cmd/server/

ENTRYPOINT [ "./bin/auth_server" ]
