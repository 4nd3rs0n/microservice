package app

import (
	"context"
	"fmt"
	"net"
	"net/http"

	httpApi "gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http"
	"golang.org/x/exp/slog"
	"google.golang.org/grpc"

	grpcApi "gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/grpc"
	pb "gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/proto"
)

func (a *App) runGrpcAPI(ctx context.Context) (err error) {
	a.logger.Info("Starting API",
		slog.String("API", "gRPC"))

	// Initialising listener
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", a.portGRPC))
	if err != nil {
		err = fmt.Errorf("Failed to start gRPC API: Failed to listen on %d port: %s", a.portHTTP, err.Error())
		return
	}

	grpcServ := grpc.NewServer()

	serv := grpcApi.NewServer(ctx, a.sdbi, a.udbi)
	pb.RegisterAuthServer(grpcServ, &serv)

	// Starting server
	err = grpcServ.Serve(listener)
	return err
}

func (a *App) runHttpAPI(ctx context.Context,
	apiPrefix string) (err error) {

	a.logger.Info("Starting API",
		slog.String("API", "HTTP"))

	apiOpts := httpApi.HttpApiOpts{
		ApiPrefix: apiPrefix,
		Users:     a.udbi,
		Sessions:  a.sdbi,
		Logger:    a.logger,
	}

	api, err := httpApi.NewApi(ctx, apiOpts)
	if err != nil {
		a.logger.Error("Failed to setup API",
			slog.String("API", "HTTP"),
			slog.String("error", err.Error()))

		return
	}
	err = http.ListenAndServe(fmt.Sprintf(":%d", a.portHTTP), api)

	if err != nil {
		a.logger.Error("API Down due the error",
			slog.String("API", "HTTP"),
			slog.String("error", err.Error()))
	}

	return err
}
