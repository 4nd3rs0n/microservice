package server

import (
	"context"

	pb "gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/proto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

type server struct {
	pb.UnimplementedAuthServer
	ctx context.Context
	ur  interfaces.UsersRepo
	sr  interfaces.SessionsRepo
}

func (s *server) Reg(ctx context.Context, in *pb.RegReq) (*pb.ErrResp, error) {
	// Check is login exists
	userOpts := entities.UserCreationOpts{
		Name: in.GetName(),
		Creds: entities.Creds{
			Login:    in.GetLogin(),
			Password: in.GetPassword(),
		},
	}

	// TODO: Validation

	// Create user
	s.ur.SaveUser(ctx, userOpts)

	//
	return &pb.ErrResp{
		ErrCode: 0,
	}, nil
}

func (s *server) Auth(ctx context.Context, in *pb.AuthReq) (*pb.AuthResp, error) {
	lgn := in.GetLogin()
	pwd := in.GetPassword()
	deviceInf := in.GetDeviceInf()

	usrID, valid, err := s.ur.VerifyLoginCreds(ctx, entities.Creds{
		Login:    lgn,
		Password: pwd,
	})
	if err != nil {
		return nil, err
	}
	if !valid {
		return nil, nil
	}
	sToken, err := s.sr.NewSession(ctx, usrID, deviceInf)

	return &pb.AuthResp{
		Token: sToken,
	}, nil
}

func (s *server) CheckAuth(ctx context.Context, in *pb.CheckAuthReq) (*pb.CheckAuthResp, error) {
	tkn := in.GetToken()
	uID, err := s.sr.GetUserIdByToken(ctx, tkn)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return &pb.CheckAuthResp{
				Userid: "",
			}, nil
		}
		return nil, err
	}
	s.ur.GetUserByID(ctx, uID)
	return &pb.CheckAuthResp{
		Userid: uID.String(),
	}, nil
}

// Constructor
func NewServer(ctx context.Context,
	sr interfaces.SessionsRepo, ur interfaces.UsersRepo) server {
	return server{
		ctx: ctx,
		ur:  ur,
		sr:  sr,
	}
}
