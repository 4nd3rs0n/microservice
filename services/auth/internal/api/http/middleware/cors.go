package middleware

import "net/http"

func EnableCORS(w *http.ResponseWriter) {
	(*w).Header().Add("Access-Control-Allow-Origin", "*")
	(*w).Header().Add("Access-Control-Allow-Methods", "*")
	(*w).Header().Add("Access-Control-Allow-Headers", "*")
}
