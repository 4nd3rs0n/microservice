BEGIN;

CREATE TABLE IF NOT EXISTS admins (
  -- Just add user IDs with admins priveleges

  -- Making admin only for this service. 
  -- There is no way for other users to know who is admin
  userId UUID REFERENCES users(id) ON DELETE CASCADE UNIQUE NOT NULL
  -- Maybe later I will add admin levels and admin password 
  -- (but maybe not) 
);

COMMIT;