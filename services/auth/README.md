# 4nd3rs0n ID
Just my authorization service for my pet projects from scratch

## About
> Maybe some features written here is not working yet. Development is still in progress 

This authorization service has HTTP (REST) and gRPC APIs out of the box and easy to add new functionality. It follows the Unix philosophy and KISS rule. Do one thing and do it good -- that's all what this service is about. It doesn't store roles, premissions for your apps, it just stores user ID, login, password hash+salt, username, sessions. Everything else can store other services for itself

It has blocklist, admins, users with 

It uses PostgreSQL for storing user's data and Redis to store sessions.

Also this service has API keys due the security reasons. There is also statistics for the service and hardwer info and healthcheck in the APIs.

## Authentication strategies
This service provides only login:password based authentication strategy for now. But, maybe latter, I will implement other types, like multi-factor 

# Running development
## Prepare service for running 
``` sh
cp example.postgres.env .postgres.env
cp example.redis.env .redis.env

# Making dev build
docker-compose -f ./docker-compose.dev.yml build
```
## Running dev
Dev container uses volumes and compile demon to rebuild project on every change. So basicaly development environment is not really isolated from host. If any file in container gets removed -- it'll be removed on your host machine as well 
``` sh
# run
docker-compose -f ./docker-compose.dev.yml up
```

# HTTP API

## /user
**POST**  
Requiers JSON body: 
- `name` string. 1-50 symbols.
- `login` string. 3-50 symbols. Only a-z 0-9 and _
- `password` string. 6-100 symbols

**GET**  
Returns information about your account.  
Requiers header:  
- `Authorization: Session <token>`

## /users/delete 
**POST**  
Removes your account.

Requiers JSON body:  
- `password` string. 6-100 symbols  

Requiers header:  
- `Authorization: Session <token>`

## /login
**POST**
Required fields in json body:
- `login` string. 3-50 symbols. Only a-z 0-9 and _
- `password` string. 6-100 symbols

## /logout
**GET**  
kills your current session

# Service Architecture
```
Request => 
Transpot (Middlewares => Handler) =>
Domain (DTOs => Business logic) =>   
Repository => 
External APIs and DataBases
```

## Transport (APIs)
Out of the box in this service exists two API implementation: **gRPC API** implementation and **REST (HTTP)**. Multiple APIs can be on at the same time.

Transport layer shouldn't call the Repository or any other layer except Domain directly, API depends on the Repository's interfaces only to inject the implementations into functions from the Domain layer.

## Domain
### DTOs
Data structures that are accepted as a parameters and returned by the business logic. Business logic is responsible for validating it.

### Business Logic
Actual application functionality, service layer of this application. 

## Repository
On this layer you'll find simple interfaces and their implementations for storage and external APIs that are used by the business logic 