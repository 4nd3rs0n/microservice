package app

import (
	"os"

	"golang.org/x/exp/slog"
)

func (a *App) initLogger(level slog.Level) *slog.Logger {
	logger := slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level:     level,
		AddSource: true,
	}))

	return logger
}
