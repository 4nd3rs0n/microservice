package username

import (
	"errors"
	"fmt"
	"regexp"
)

var (
	ErrInvalidUsernameBase = errors.New("Invalid username base")
)

var spliter string = ":"

// Username:
// (@)<base>:<id>
type Username interface {
	// 4nd3rs0n
	IsNil() bool
	Base() string
	// 666
	ID() int

	// @4nd3rs0n:666
	String() string
	// 4nd3rs0n:666
	StringWithoutAt() string
}

type username struct {
	base string
	id   int
}

func NewUsername(base string, id int) (Username, error) {
	if base == "" && id == 0 {
		return &NilUsername{}, nil
	}
	if match, err := regexp.MatchString(UsernameBaseRegexp, base); !match {
		if err != nil {
			return nil, err
		}
		return nil, ErrInvalidUsernameBase
	}
	return &username{
		base: base,
		id:   id,
	}, nil
}

func (u *username) IsNil() bool {
	return false
}
func (u *username) Base() string {
	return u.base
}
func (u *username) ID() int {
	return u.id
}
func (u *username) String() string {
	return fmt.Sprintf("@%s%s%d", u.base, spliter, u.id)
}
func (u *username) StringWithoutAt() string {
	return fmt.Sprintf("%s%s%d", u.base, spliter, u.id)
}

type NilUsername struct{}

func (nu *NilUsername) IsNil() bool {
	return true
}
func (nu *NilUsername) Base() string {
	return ""
}
func (nu *NilUsername) ID() int {
	return 0
}
func (nu *NilUsername) String() string {
	return ""
}
func (nu *NilUsername) StringWithoutAt() string {
	return fmt.Sprintf("")
}
