package middleware

import (
	"errors"
	"net/http"
	"strings"
)

var ErrNoToken = errors.New("No token")
var ErrBadRequest = errors.New("Bad request")
var ErrWrongAuthType = errors.New("Wrong auth type")

func GetAuthToken(r *http.Request) (tkn string, err error) {
	auth := r.Header.Get("Authorization")
	if auth == "" {
		return "", nil
	}
	authSplited := strings.Split(auth, " ")
	if len(authSplited) != 2 {
		return "", ErrBadRequest
	}
	if strings.ToLower(authSplited[0]) != "session" {
		return "", ErrWrongAuthType
	}

	tkn = authSplited[1]
	return tkn, nil
}

func TokenPrehandler(w http.ResponseWriter, r *http.Request) (tkn string, next bool, err error) {
	usrTkn, err := GetAuthToken(r)
	if err != nil {
		switch err {
		case ErrBadRequest, ErrWrongAuthType:
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Wrong usage of the Authorization header.\n" +
				"Use it as: Authorization: Session <token>"))
		case ErrNoToken:
			w.WriteHeader(http.StatusUnauthorized)
		default:
			w.WriteHeader(http.StatusInternalServerError)
			return "", false, err
		}
		return "", false, nil
	}
	return usrTkn, true, nil
}
