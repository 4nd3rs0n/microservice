package sessions_api

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/logic"
)

func (sa *SessionsAPI) KillSession(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	token, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	var sID uuid.UUID
	reqTknOrSID := chi.URLParam(r, "tknOrID")
	sID, err = uuid.Parse(reqTknOrSID)
	if err != nil {
		sID, err = sa.sr.GetSessionIdByToken(ctx, reqTknOrSID)
		if err != nil {
			return
		}
	}
	opts := dto.KillSessionOpts{
		Token: token,
		SID:   sID,
	}
	errx := logic.KillSession(ctx, opts, sa.sr)
	if errx != nil {
		logic.HTTPHandleError(sa.logger, w, r, errx)
		return
	}

	w.WriteHeader(http.StatusNoContent)
	w.Write([]byte("Deleted successfully"))
}
