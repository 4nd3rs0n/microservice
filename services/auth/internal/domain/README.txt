Here is located Service layer of this backend
Contains bussiness logic and actual application functionality

Domain depends on the repository interfaces and types 
Implementations of the repository interfaces are 
injected into the parameters

All the data validation also happens on this level