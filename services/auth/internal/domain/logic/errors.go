package logic

import (
	"net/http"

	"gitlab.com/4nd3rs0n/errorsx"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/logpkg"
	"golang.org/x/exp/slog"
)

// Business Logic Error
type BLError errorsx.CustomErr[ErrInf]

type ErrInf interface {
	// Anything that you need to log.
	// Like: the place of an error,
	// some variables and the other stuff
	Log() bool
	States() map[string]any

	// Sometimes it's important to not show the real error
	// by the security reasons
	// Left it empty to show the true error
	// Default: empty string
	HTTPResp() string
	// Default: http.StatusInternalServerError
	HTTPCode() int
}

type errInf struct {
	log    bool
	states map[string]any
	// http
	httpResp string
	httpCode int
}

func (einf *errInf) Log() bool {
	return einf.log
}
func (einf *errInf) States() map[string]any {
	return einf.states
}
func (einf *errInf) HTTPCode() int {
	return einf.httpCode
}
func (einf *errInf) HTTPResp() string {
	return einf.httpResp
}

type ErrOpts struct {
	Log    bool
	States map[string]any

	HTTPResp string
	HTTPCode int
}

func newErrInfo(opts ErrOpts) ErrInf {
	return &errInf{
		log:      opts.Log,
		states:   opts.States,
		httpResp: opts.HTTPResp,
		httpCode: opts.HTTPCode,
	}
}

func NewErr(err string, opts ErrOpts) BLError {
	if opts.HTTPCode == 0 {
		opts.HTTPCode = http.StatusInternalServerError
	}
	einf := newErrInfo(opts)
	return errorsx.NewCustomErr(err, einf)
}

func NewValidationErr(err string, addStates map[string]any) BLError {
	states := map[string]any{
		"in": ERR_IN_VALIDATION}
	for key, val := range addStates {
		states[key] = val
	}

	einf := newErrInfo(ErrOpts{
		Log:      false,
		States:   states,
		HTTPCode: http.StatusBadRequest,
	})
	return errorsx.NewCustomErr(err, einf)
}

func NewRepositoryErr(err string, httpErr string,
	addStates map[string]any) BLError {

	states := map[string]any{
		"in": ERR_IN_REPOSITORY}
	for key, val := range addStates {
		states[key] = val
	}

	einf := newErrInfo(ErrOpts{
		Log:      true,
		States:   states,
		HTTPResp: httpErr,
		HTTPCode: http.StatusInternalServerError,
	})
	return errorsx.NewCustomErr(err, einf)
}

func TokenIsInvalidErr() BLError {
	return NewErr(ErrTokenInvalid, ErrOpts{
		Log:      false,
		States:   map[string]any{},
		HTTPCode: http.StatusUnauthorized,
	})
}

// Error codes
const (
	ERR_IN_VALIDATION       = "validation"
	ERR_IN_PERMISSION_CHECK = "premission check"
	// Errors returned from the external APIs or DBs
	ERR_IN_REPOSITORY = "repository"
)

// Texts for error messages
const (
	ErrLoginFailed      = "Login failed. Credentials are invalid"
	ErrLoginTaken       = "This login is already taken. Try another"
	ErrTokenInvalid     = "Token is invalid"
	ErrPermissionDenied = "Premission denied"

	ErrEntityExists   = "Entity already exists"
	ErrEntityNotFound = "Entity wasn't found"

	ErrTokenLength = "Invalid token length"
)

// Error HTTP Handler
func HTTPHandleError(
	logger *slog.Logger,
	w http.ResponseWriter,
	r *http.Request,
	err errorsx.CustomErr[ErrInf]) {

	einf := err.Info()
	if einf.HTTPResp() == "" {
		w.Write([]byte(err.Error()))
	} else {
		w.Write([]byte(einf.HTTPResp()))
	}
	w.WriteHeader(einf.HTTPCode())

	if einf.Log() {
		var log []any
		log = append(log, mapToSlog(einf.States())...)
		logRequest := logpkg.RequestToSlog(r, logpkg.RequestToSlogCfg{
			LogURL:     true,
			LogHeaders: true,
			LogBody:    true,
		})
		log = append(log, logRequest...)

		logger.Error(err.Error(), log...)
	}
}

func mapToSlog(in map[string]any) []any {
	var log []any
	for key, val := range in {
		log = append(log, slog.Any(key, val))
	}
	return log
}
