package users

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/username"
)

func (p *PostgresInterface) GetFreeUsernameId(
	ctx context.Context, username string) (int, error) {

	var id int
	err := p.pool.QueryRow(ctx,
		"COALESCE("+
			"(SELECT MAX(username_id) "+
			"FROM user_data "+
			"WHERE username = $1), 0)", username).Scan(&id)
	return id, err
}

func (p *PostgresInterface) SetUsername(
	ctx context.Context, uID uuid.UUID, username string) error {

	_, err := p.pool.Exec(ctx,
		"UPDATE user_data "+
			"SET username = $2, "+
			"username_id = "+
			("COALESCE("+
				"(SELECT MAX(username_id) "+
				"FROM user_data "+
				"WHERE username = $2), 0) ")+
			"WHERE userId = $1", uID, username)
	return err
}

func (p *PostgresInterface) SetNilUsername(
	ctx context.Context, uID uuid.UUID) error {

	_, err := p.pool.Exec(ctx,
		"UPDATE user_data "+
			"SET username = $2, "+
			"username_id = $2"+
			"WHERE userId = $1", uID, nil)
	return err
}

func (p *PostgresInterface) GetUsernameByID(
	ctx context.Context, uID uuid.UUID) (username.Username, error) {

	var base string
	var id int
	err := p.pool.QueryRow(ctx,
		"SELECT username, username_id "+
			"FROM user_data "+
			"WHERE userId = $1", uID).Scan(&base, &id)

	if err != nil {
		return nil, err
	}
	return username.NewUsername(base, id)
}

func (p *PostgresInterface) GetIdByUsername(
	ctx context.Context, uname username.Username) (uuid.UUID, error) {

	var uid uuid.UUID
	err := p.pool.QueryRow(ctx,
		"SELECT userId "+
			"FROM user_data "+
			"WHERE username = $1 "+
			"AND username_id = $2", uname.Base(), uname.ID()).Scan(&uid)
	return uid, err
}
