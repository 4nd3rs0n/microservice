package users

import (
	"context"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

// Creates new user
func (p *PostgresInterface) SaveUser(
	ctx context.Context,
	user entities.UserCreationOpts) (uuid.UUID, error) {

	var uid uuid.UUID

	passwordHash, salt, err := hashPassword(user.Password)
	if err != nil {
		return uuid.Nil, err
	}

	tx, err := p.pool.Begin(ctx)
	if err != nil {
		return uuid.Nil, err
	}

	err = tx.QueryRow(ctx, ""+
		"INSERT INTO public.users DEFAULT VALUES RETURNING id;").Scan(&uid)
	if err != nil {
		tx.Rollback(ctx)
		return uuid.Nil, err
	}
	_, err = tx.Exec(ctx, ""+
		"INSERT INTO public.creds (userId, login, password_hash, salt) "+
		"VALUES ($1, $2, $3, $4);", uid, user.Creds.Login, passwordHash, salt)
	if err != nil {
		tx.Rollback(ctx)
		// Check for duplicate
		if strings.Contains(err.Error(), "duplicate key") {
			return uuid.Nil, entities.ErrLoginTaken
		}
		return uuid.Nil, err
	}
	_, err = tx.Exec(ctx, ""+
		"INSERT INTO public.user_data (userId, displayed_name) "+
		"VALUES ($1, $2)", uid, user.Name)
	if err != nil {
		tx.Rollback(ctx)
		return uuid.Nil, err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return uuid.Nil, err
	}

	return uid, nil
}

// Deletes the user or returns an error
func (p *PostgresInterface) RemoveUser(ctx context.Context, id uuid.UUID) error {
	_, err := p.pool.Exec(ctx, "DELETE FROM public.users WHERE id = $1;", id)
	return err
}
