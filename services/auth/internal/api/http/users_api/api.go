package users_api

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
	"golang.org/x/exp/slog"
)

type UsersAPI struct {
	ur     interfaces.UsersRepo
	sr     interfaces.SessionsRepo
	logger *slog.Logger
}

func NewUsersAPI(
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo,
	logger *slog.Logger) UsersAPI {

	return UsersAPI{
		ur:     ur,
		sr:     sr,
		logger: logger,
	}
}

func (ua *UsersAPI) API(ctx context.Context, r *chi.Mux) {
	r.Route("/users", func(r chi.Router) {
		r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			switch r.Method {
			case http.MethodOptions:
				w.WriteHeader(http.StatusOK)
			case http.MethodPost:
				ua.postUserH(ctx, w, r)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})

		r.HandleFunc("/{userID}", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			reqUIDstr := chi.URLParam(r, "userID")
			reqUID, err := uuid.Parse(reqUIDstr)
			if err != nil {
				w.WriteHeader(http.StatusNotFound)
				return
			}

			switch r.Method {
			case http.MethodOptions:
				w.WriteHeader(http.StatusOK)
			case http.MethodGet:
				ua.getUserH(ctx, w, r)
			case http.MethodDelete:
				ua.deleteUserH(ctx, w, r, reqUID)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})
	})

	r.Route("/me", func(r chi.Router) {
		r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)
			switch r.Method {
			case http.MethodOptions:
				w.WriteHeader(http.StatusOK)
			case http.MethodGet:
				ua.getMeH(ctx, w, r)
			case http.MethodDelete:
				ua.deleteMeH(ctx, w, r)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})
		r.HandleFunc("/username", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)
			switch r.Method {
			case http.MethodOptions:
				w.WriteHeader(http.StatusOK)
			case http.MethodPatch:
				ua.SetUsernameH(ctx, w, r)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})
		r.HandleFunc("/name", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)
			switch r.Method {
			case http.MethodOptions:
				w.WriteHeader(http.StatusOK)
			case http.MethodPatch:
				ua.ChangeNameH(ctx, w, r)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})
		r.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)
			switch r.Method {
			case http.MethodOptions:
				w.WriteHeader(http.StatusOK)
			case http.MethodPatch:
				ua.ChangeLoginH(ctx, w, r)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})
		r.HandleFunc("/password", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)
			switch r.Method {
			case http.MethodOptions:
				w.WriteHeader(http.StatusOK)
			case http.MethodPatch:
				ua.ChangePasswordH(ctx, w, r)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})
	})
}
