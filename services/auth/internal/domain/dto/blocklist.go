package dto

import "gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/validate"

// Used to unban user as well
type BanOpts struct {
	Token         string
	UIDorUsername string
}

func (opts *BanOpts) Validate() error {
	err := validate.ValidateSessionToken(opts.Token)
	return err
}
