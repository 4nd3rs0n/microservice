package users_api

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/logic"
)

type ChangeLoginReqBody struct {
	Password string `json:"password"`
	NewLogin string `json:"newLogin"`
}

func (api *UsersAPI) ChangeLoginH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	token, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	var body ChangeLoginReqBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	errx := logic.ChangeLogin(ctx, dto.ChangeLoginOpts{
		Token:    token,
		Password: body.Password,
		NewLogin: body.NewLogin,
	}, api.ur, api.sr)
	if errx != nil {
		logic.HTTPHandleError(api.logger, w, r, errx)
		return
	}

	w.WriteHeader(http.StatusOK)
}

type ChangePasswordReqBody struct {
	OldPassword string `json:"oldPassword"`
	NewPassword string `json:"newPassword"`
}

func (api *UsersAPI) ChangePasswordH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	token, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	var body ChangePasswordReqBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	errx := logic.ChangePassword(ctx, dto.ChangePasswordOpts{
		Token:       token,
		OldPassord:  body.OldPassword,
		NewPassword: body.NewPassword,
	}, api.ur, api.sr)
	if errx != nil {
		logic.HTTPHandleError(api.logger, w, r, errx)
		return
	}

	w.WriteHeader(http.StatusOK)
}

type ChangeNameReqBody struct {
	Password string `json:"password"`
	NewName  string `json:"newName"`
}

// Handles PATCH {prefix}/me/name
func (api *UsersAPI) ChangeNameH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	token, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	var body ChangeNameReqBody
	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	errx := logic.ChangeDisplayedName(ctx, dto.ChangeNameOpts{
		Token:   token,
		NewName: body.NewName,
	}, api.ur, api.sr)
	if errx != nil {
		logic.HTTPHandleError(api.logger, w, r, errx)
		return
	}

	w.WriteHeader(http.StatusOK)
}
