package validate

import "errors"

var (
	ErrTooShort = errors.New("Too short")
	ErrTooLong  = errors.New("Too long")

	ErrInvalidTokenLen = errors.New("Invalid token length")
)
