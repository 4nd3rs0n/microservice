package logic

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

// Removes user. If user deleted -- returns nil, otherwise error
func RemoveUser(
	ctx context.Context,
	opts dto.RemoveUserOpts,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo) BLError {

	if err := opts.Validate(); err != nil {
		return NewValidationErr(err.Error(), map[string]any{})
	}

	uID, blerr := getUserIdByToken(ctx, opts.Token, sr)
	if blerr != nil {
		return blerr
	}
	reqUID, blerr := uuidOrUsername(ctx, opts.UIDorUsername, ur)
	if blerr != nil {
		return blerr
	}

	sameUser := uID == reqUID
	// TODO: Admins table in DB and admin check
	admin, blerr := IsAdmin(ctx, uID, ur)
	if blerr != nil {
		return blerr
	}

	if !sameUser && !admin {
		return NewErr(ErrPermissionDenied, ErrOpts{
			Log: false,
			States: map[string]any{
				"in":     ERR_IN_PERMISSION_CHECK,
				"uid":    uID,
				"reqUID": reqUID},
			HTTPCode: http.StatusForbidden,
		})
	}

	return removeUser(ctx, reqUID, ur, sr)
}

func RemoveMe(ctx context.Context, token string,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo) BLError {

	if len(token) != entities.TOKEN_LENGTH {
		return NewValidationErr(ErrTokenLength, map[string]any{})
	}
	uID, blerr := getUserIdByToken(ctx, token, sr)
	if blerr != nil {
		return blerr
	}

	return removeUser(ctx, uID, ur, sr)
}

func removeUser(ctx context.Context,
	uid uuid.UUID, ur interfaces.UsersRepo, sr interfaces.SessionsRepo) BLError {

	err := ur.RemoveUser(ctx, uid)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return NewErr(ErrEntityNotFound, ErrOpts{
				Log:      false,
				HTTPResp: "Failed to remove: User not found",
				HTTPCode: http.StatusNotFound,
				States:   map[string]any{"in": ERR_IN_REPOSITORY, "uid": uid},
			})
		}
		return NewRepositoryErr(err.Error(),
			"Failed to remove user due the unexpected error", map[string]any{})
	}
	_, err = sr.KillAllSessions(ctx, uid)
	if err != nil {
		return NewRepositoryErr(err.Error(),
			"Account deleted, but failed to kill sessions "+
				"due the unexpected error", map[string]any{"uid": uid})
	}
	return nil
}
