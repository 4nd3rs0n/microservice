package logpkg

import (
	"context"

	"golang.org/x/exp/slog"
)

type NewLogToFileHandlerOpts struct {
	LogLvl   slog.Level
	FilePath string
}

type LogToFileHandler struct {
	logLvl   slog.Level
	filePath string
}

func NewLogToFileHandler(ctx context.Context, opts NewLogToFileHandlerOpts) LogToFileHandler {
	return LogToFileHandler{
		logLvl:   opts.LogLvl,
		filePath: opts.FilePath,
	}
}
