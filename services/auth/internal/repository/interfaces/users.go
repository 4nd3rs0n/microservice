package interfaces

import (
	"context"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	users "gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/implementations/users_psql"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/username"
)

type UsersRepo interface {
	SaveUser(context.Context, entities.UserCreationOpts) (uID uuid.UUID, err error)
	RemoveUser(ctx context.Context, uID uuid.UUID) error

	GetUserByID(ctx context.Context, uID uuid.UUID) (entities.User, error)
	GetUserIdByLogin(ctx context.Context, login string) (uID uuid.UUID, err error)

	VerifyLoginCreds(context.Context, entities.Creds) (uID uuid.UUID, valid bool, err error)
	VerifyIdAndPassword(ctx context.Context, uid uuid.UUID, password string) (valid bool, err error)

	IsLoginTaken(ctx context.Context, login string) (isFree bool, err error)

	GetCountOfUsers(ctx context.Context) (count int, err error)

	// Change the user
	ChangeLogin(ctx context.Context, uID uuid.UUID, newLogin string) error
	ChangePassword(ctx context.Context, uID uuid.UUID, newPassword string) error
	ChangeDisplayedName(ctx context.Context, uID uuid.UUID, newName string) error

	// Manipulations with the admin table:
	MakeAdmin(ctx context.Context, uID uuid.UUID) error
	IsAdmin(ctx context.Context, uID uuid.UUID) (admin bool, err error)
	GetAllAdmins(ctx context.Context) (admins []entities.UserWithoutLogin, err error)

	// Actions to ban or unban someone
	BanUser(ctx context.Context, bannedID uuid.UUID, bannedByID uuid.UUID) error
	UnbanUser(ctx context.Context, bannedID uuid.UUID, bannedByID uuid.UUID) error
	IsBanned(ctx context.Context, bannedID uuid.UUID, bannedByID uuid.UUID) (bool, error)

	// TODO
	// GetByWhoBannedUserID(ctx context.Context,)
	// GetBannedByUserID(ctx context.Context,)

	// Username actions
	GetFreeUsernameId(ctx context.Context, username string) (id int, err error)
	SetUsername(ctx context.Context, uID uuid.UUID, username string) error
	SetNilUsername(ctx context.Context, uID uuid.UUID) error
	GetUsernameByID(ctx context.Context, uID uuid.UUID) (username.Username, error)
	GetIdByUsername(ctx context.Context, username username.Username) (uid uuid.UUID, err error)
}

func NewUsersRepo(pool *pgxpool.Pool) UsersRepo {
	return users.NewPostrgerInterface(pool)
}
