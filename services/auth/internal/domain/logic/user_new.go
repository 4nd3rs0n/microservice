package logic

import (
	"context"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

// Creates new user, returns session token to manipulate this user
func CreateUser(
	ctx context.Context,
	user dto.CreateUserOpts,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo,
) (dto.CreateUserResp, BLError) {

	var resp dto.CreateUserResp

	if err := user.Validate(); err != nil {
		return resp, NewValidationErr(err.Error(), map[string]any{})
	}

	opts := entities.UserCreationOpts{
		Name: user.Name,
		Creds: entities.Creds{
			Login:    user.Login,
			Password: user.Password,
		},
	}
	uID, err := ur.SaveUser(ctx, opts)
	if err != nil {
		if err == entities.ErrLoginTaken {
			return resp, NewErr(err.Error(), ErrOpts{
				Log:      false,
				HTTPCode: http.StatusConflict,
				States: map[string]any{
					"in": ERR_IN_REPOSITORY},
			})
		}
		return resp, NewRepositoryErr(err.Error(),
			"Failed to create new user due the unexpected error",
			map[string]any{})
	}
	sessionToken, err := sr.NewSession(ctx, uID, user.DeviceInfo)
	if err != nil {
		return resp, NewRepositoryErr(err.Error(),
			"Failed to create session for you due the unexpected error. "+
				"User created without errors, try to login manually",
			map[string]any{"uid": uID})
	}
	resp.Token = sessionToken
	return resp, nil
}
