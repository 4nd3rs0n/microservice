package validate

import (
	"errors"
	"fmt"
	"regexp"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/username"
)

func validateLen(a string, min int, max int) error {
	length := len(a)
	if length < min {
		return fmt.Errorf("%s. Minimum: %d", ErrTooShort.Error(), min)
	}
	if length > max {
		return fmt.Errorf("%s. Maximum: %d", ErrTooLong.Error(), max)
	}
	return nil
}

func ValidateName(name string) error {
	if err := validateLen(name, MIN_NAME_LEN, MAX_NAME_LEN); err != nil {
		return err
	}
	if match, _ := regexp.MatchString(Name, name); !match {
		return fmt.Errorf("Doesn't match for a reqexp: %s", Name)
	}
	return nil
}

func ValidateLogin(login string) error {
	if err := validateLen(login, MIN_LOGIN_LEN, MAX_LOGIN_LEN); err != nil {
		return err
	}
	if match, _ := regexp.MatchString(Login, login); !match {
		return fmt.Errorf("Doesn't match for a reqexp: %s", Login)
	}
	return nil
}

func ValidatePassword(password string) error {
	length := len(password)
	if length < MIN_PASSWORD_LEN {
		return fmt.Errorf("%s. Minimum: %d", ErrTooShort.Error(),
			MIN_PASSWORD_LEN)
	}
	return nil
}

func ValidateDeviceInfo(deviceInfo string) error {
	if len(deviceInfo) > MAX_DEVICE_INF_LEN {
		return fmt.Errorf("%s. Minimum: %d", ErrTooLong.Error(), MAX_DEVICE_INF_LEN)
	}
	return nil
}

func ValidateIP(ip string) error {
	if match, _ := regexp.MatchString(IpAddr, ip); !match {
		return fmt.Errorf("Doesn't match for a reqexp: %s", IpAddr)
	}
	return nil
}

func ValidateSessionToken(token string) error {
	if len(token) != entities.TOKEN_LENGTH {
		return ErrInvalidTokenLen
	}
	return nil
}

func ValidateUsernameBase(uname string) error {
	if match, err := regexp.MatchString(username.UsernameBaseRegexp, uname); !match {
		if err != nil {
			return err
		}
		return errors.New("Invalid username")
	}
	return nil
}
