package sessions

import (
	"context"

	"github.com/google/uuid"
	redis "github.com/redis/go-redis/v9"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

func (ri *RedisInterface) KillSession(ctx context.Context, sID uuid.UUID) error {
	_, err := (*ri.Client).Pipelined(ctx, func(pipe redis.Pipeliner) error {
		tokenHash, err := (*ri.Client).HGet(ctx, "sessions.id."+sID.String(), "sth").Result()
		if err != nil {
			if err == redis.Nil {
				return entities.ErrEntityNotFound
			}
			return err
		}
		uIDstr, err := (*ri.Client).HGet(ctx, "sessions.id."+sID.String(), "uid").Result()
		if err != nil {
			return err
		}
		uID, err := uuid.Parse(uIDstr)
		if err != nil {
			return err
		}
		err = (*ri.Client).Del(ctx, "sessions.id."+sID.String()).Err()
		if err != nil && err != redis.Nil {
			return err
		}
		err = (*ri.Client).Del(ctx, "sessions.token."+tokenHash).Err()
		if err != nil && err != redis.Nil {
			return err
		}
		err = (*ri.Client).SRem(ctx, "users."+uID.String()+".sessions", sID.String()).Err()
		if err != nil {
			return err
		}

		return nil
	})

	return err
}

func (ri *RedisInterface) KillAllSessions(ctx context.Context, usrId uuid.UUID) (int, error) {
	var killed int = 0
	result, err := (*ri.Client).SMembers(ctx, "users."+usrId.String()+".sessions").Result()
	if err != nil {
		return killed, err
	}

	for _, sIDstr := range result {
		tokenHash, err := (*ri.Client).HGet(ctx, "sessions.id."+sIDstr, "sth").Result()
		if err != nil {
			return killed, err
		}
		err = (*ri.Client).Del(ctx, "sessions.id."+sIDstr).Err()
		if err != nil && err != redis.Nil {
			return killed, err
		}
		err = (*ri.Client).Del(ctx, "sessions.token."+tokenHash).Err()
		if err != nil && err != redis.Nil {
			return killed, err
		}
		err = (*ri.Client).SRem(ctx, "users."+usrId.String()+".sessions", sIDstr).Err()
		if err != nil {
			return killed, err
		}

		killed++
	}
	return killed, nil
}
