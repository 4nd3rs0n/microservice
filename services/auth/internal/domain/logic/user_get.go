package logic

import (
	"context"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

func GetMe(ctx context.Context, token string,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo) (dto.GetMeResp, BLError) {

	var resp dto.GetMeResp
	if len(token) != entities.TOKEN_LENGTH {
		return resp, NewValidationErr(ErrTokenLength, map[string]any{})
	}

	uid, blerr := getUserIdByToken(ctx, token, sr)
	if blerr != nil {
		return resp, blerr
	}

	user, err := ur.GetUserByID(ctx, uid)
	if err != nil {
		return resp, NewRepositoryErr(err.Error(),
			"Failed to get your account info due the unexpected error",
			map[string]any{"uid": uid})
	}

	resp.ID = user.ID
	resp.Username = user.Username.String()
	resp.Login = user.Login
	resp.Name = user.Name
	resp.CreatedAt = user.CreatedAt
	return resp, nil
}

// It returns not the struct but map,
// because values in responce depends on
func GetUser(
	ctx context.Context,
	opts dto.GetUserOpts,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo,
) (dto.GetUserResp, map[string]any, BLError) {

	resp := dto.GetUserResp{}
	mapResp := map[string]any{}

	reqUID, blerr := uuidOrUsername(ctx, opts.UIDorUsername, ur)
	if blerr != nil {
		return resp, mapResp, blerr
	}

	reqUser, err := ur.GetUserByID(ctx, reqUID)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return resp, mapResp, NewErr(ErrEntityNotFound, ErrOpts{
				Log: false,
				States: map[string]any{"in": ERR_IN_REPOSITORY,
					"requested_uid": reqUID},
				HTTPResp: "Requested user wasn't found",
				HTTPCode: http.StatusNotFound,
			})
		}
		return resp, mapResp, NewRepositoryErr(err.Error(),
			"",
			map[string]any{"requested_uid": reqUID})
	}

	// Without token you
	if len(opts.Token) == 0 {
		resp.ID = reqUser.ID
		resp.Name = reqUser.Name
		resp.Username = reqUser.Username
		mapResp["id"] = resp.ID.String()
		mapResp["name"] = reqUser.Name
		mapResp["username"] = reqUser.Username.String()
		return resp, mapResp, nil
	}

	if len(opts.Token) != entities.TOKEN_LENGTH {
		return resp, mapResp, NewValidationErr(ErrTokenLength, map[string]any{})
	}
	uid, blerr := getUserIdByToken(ctx, opts.Token, sr)
	if blerr != nil {
		return resp, mapResp, blerr
	}
	banned, err := ur.IsBanned(ctx, reqUID, uid)
	if err != nil {
		return resp, mapResp, NewRepositoryErr(err.Error(),
			"Failed to check your privilegies due the unexpected error",
			map[string]any{})
	}
	if banned {
		return resp, mapResp, NewErr(ErrPermissionDenied, ErrOpts{
			Log:      false,
			States:   map[string]any{"in": ERR_IN_PERMISSION_CHECK},
			HTTPResp: "You've been banned by this user",
			HTTPCode: http.StatusForbidden})
	}

	isBannedByYou, err := ur.IsBanned(ctx, reqUID, uid)
	if err != nil {
		return resp, mapResp, NewRepositoryErr(err.Error(),
			"Failed to check is requested user banned or not "+
				"due the unexpected error",
			map[string]any{})
	}

	resp = dto.GetUserResp{
		ID:        reqUser.ID,
		Name:      reqUser.Name,
		Username:  reqUser.Username,
		Banned:    isBannedByYou,
		CreatedAt: reqUser.CreatedAt,
	}
	mapResp = map[string]any{
		"id":        resp.ID.String(),
		"name":      reqUser.Name,
		"username":  reqUser.Username.String(),
		"banned":    isBannedByYou,
		"createdAt": reqUser.CreatedAt,
	}
	return resp, mapResp, nil
}
