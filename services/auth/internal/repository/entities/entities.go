package entities

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/username"
)

type Creds struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type Session struct {
	SID       uuid.UUID `json:"sessionID"`
	UID       uuid.UUID `json:"usrID"`
	CreatedAt int64     `json:"createdAt"`
	DeviceInf string    `json:"deviceInf"`
	CreatorIP string    `json:"creatorIP"`
}

type User struct {
	ID        uuid.UUID         `json:"id"`
	Username  username.Username `jsom:"username"`
	Login     string            `json:"login"`
	Name      string            `json:"name"`
	CreatedAt time.Time
}

type UserCreationOpts struct {
	Name string
	Creds
}

type UserWithoutLogin struct {
	ID        uuid.UUID `json:"id"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"createdAt"`
}
