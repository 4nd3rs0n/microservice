BEGIN;

CREATE TABLE IF NOT EXISTS blocklist (
  banned_id UUID REFERENCES users(id) ON DELETE CASCADE NOT NULL,
  banned_by_id UUID REFERENCES users(id) ON DELETE CASCADE NOT NULL,
  CONSTRAINT unique_ban_pair UNIQUE (banned_id, banned_by_id)
);

COMMIT;

