package logic

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

func getUserIdByToken(
	ctx context.Context,
	token string,
	sr interfaces.SessionsRepo) (uuid.UUID, BLError) {

	uID, err := sr.GetUserIdByToken(ctx, token)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return uuid.Nil, NewErr(ErrTokenInvalid, ErrOpts{
				Log: false,
				States: map[string]any{
					"in": ERR_IN_REPOSITORY},
				HTTPCode: http.StatusUnauthorized,
			})
		}
		return uuid.Nil, NewRepositoryErr(err.Error(),
			"Failed to get your User ID due the unexpected error",
			map[string]any{})
	}

	return uID, nil
}

func getSessionIdByToken(
	ctx context.Context,
	token string,
	sr interfaces.SessionsRepo) (uuid.UUID, BLError) {

	sID, err := sr.GetUserIdByToken(ctx, token)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return uuid.Nil, NewErr(ErrTokenInvalid, ErrOpts{
				Log: false,
				States: map[string]any{
					"in": ERR_IN_REPOSITORY},
				HTTPCode: http.StatusUnauthorized,
			})
		}
		return uuid.Nil, NewRepositoryErr(err.Error(),
			"Failed to get your Session ID due the unexpected error",
			map[string]any{})
	}

	return sID, nil
}
