package logpkg

import (
	"context"

	"golang.org/x/exp/slog"
)

// TODO

// This handler sents messages via telegram
type TelegramSlogOpts struct {
	BotApiKey string
	UserIDs   []string
	ChatIDs   []string

	LogLvl slog.Level
}

type TelegramSlogHandler struct {
}

func NewTelegramSlogHandler(ctx context.Context, opts TelegramSlogOpts) TelegramSlogHandler {
	return TelegramSlogHandler{}
}
