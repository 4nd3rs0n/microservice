package entities

import (
	"errors"
	"fmt"
)

var (
	ErrEntityNotFound = errors.New("Entity wasn't found")
	ErrEntityExists   = errors.New("Entity already exist")
)

var (
	ErrTokenLen = fmt.Errorf("Token length should be equal to %d", TOKEN_LENGTH)
)

var (
	ErrLoginTaken  = errors.New("Login is already taken")
	ErrNoChange    = errors.New("No changes")
	ErrBanYourself = errors.New("You cannot ban yourself")
)
