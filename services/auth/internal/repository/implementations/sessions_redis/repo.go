package sessions

import (
	"crypto"
	_ "crypto/sha256"
	"encoding/hex"

	redis "github.com/redis/go-redis/v9"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

// TODO: Make sessions to contain the following information:
// DONE token: <string>
// DONE device: <string>
// last use ip: <255.255.255.255>
// last use: <date><time>
// DONE created by ip: <255.255.255.255>
// DONE created at: <date><time>
// expire at: <date><time>

func HashToken(tkn string) (tokenHash string, err error) {
	if len([]byte(tkn)) != entities.TOKEN_LENGTH {
		return "", entities.ErrTokenLen
	}
	tokenBytes := make([]byte, entities.TOKEN_BYTES_LENGTH, entities.TOKEN_BYTES_LENGTH)
	hex.Decode(tokenBytes, []byte(tkn))

	sha := crypto.SHA256.New()
	sha.Write(tokenBytes)
	hashTokenBytes := sha.Sum(nil)
	tokenHashStr := hex.EncodeToString(hashTokenBytes)

	return tokenHashStr, nil
}

type RedisInterface struct {
	Client *redis.Client
}
