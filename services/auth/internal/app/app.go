package app

import (
	"context"
	"sync"

	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/redis/go-redis/v9"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
	"golang.org/x/exp/slog"
)

type Config struct {
	StartHTTP     bool
	HTTPport      int
	HttpApiPrefix string

	StartGRPC bool
	GRPCport  int

	PgURL         string
	MigrationPath string
	PgEnableSSL   bool

	RedisURL      string
	RedisPassword string
	RedisBD       int

	// If there is no admins yet it will automatically create it
	AdminLogin    string
	AdminPassowrd string

	Stage string
}

type App struct {
	pgPool *pgxpool.Pool
	rc     *redis.Client

	udbi   interfaces.UsersRepo
	sdbi   interfaces.SessionsRepo
	logger *slog.Logger

	runHTTP  bool
	runGRPC  bool
	portHTTP int
	portGRPC int

	// Prefferences
	logAllRequests bool
	logInternalErr bool

	// App statements
	stop chan bool
}

func InitApp(ctx context.Context, cfg Config) (App, error) {
	app := App{
		stop: make(chan bool),
	}
	app.runHTTP = cfg.StartHTTP
	app.runGRPC = cfg.StartGRPC
	app.portGRPC = cfg.GRPCport
	app.portHTTP = cfg.HTTPport

	var logLevel slog.Level
	var pgURL string = cfg.PgURL
	switch cfg.Stage {
	case "dev", "test":
		logLevel = slog.LevelDebug
		if !cfg.PgEnableSSL {
			pgURL += "?sslmode=disable"
		}
	case "prod":
		logLevel = slog.LevelInfo
	default:
		logLevel = slog.LevelDebug
	}

	app.logger = app.initLogger(logLevel)

	err := app.initDBs(ctx, pgURL, cfg.MigrationPath,
		cfg.RedisURL, cfg.RedisPassword, cfg.RedisBD)
	return app, err
}

func (a *App) Run(ctx context.Context) {
	var wg sync.WaitGroup

	if a.runGRPC {
		wg.Add(1)
		go func() {
			a.runGrpcAPI(ctx)
			wg.Done()
		}()
	}
	if a.runHTTP {
		wg.Add(1)
		go func() {
			a.runHttpAPI(ctx, "/")
		}()
	}

	done := make(chan bool)
	go func() {
		wg.Wait()
		close(done)
	}()

	select {
	case <-a.stop:
		return
	case <-done:
		return
	}
}

func (a *App) Close(ctx context.Context) {
	close(a.stop)

	a.rc.Close()
	a.pgPool.Close()
}
