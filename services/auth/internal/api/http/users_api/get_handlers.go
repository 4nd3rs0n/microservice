package users_api

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/logic"
)

type GetMeRespBody struct {
	ID        string `json:"id"`
	Login     string `json:"login"`
	Username  string `json:"username"`
	Name      string `json:"name"`
	CreatedAt string `json:"createdAt"`
}

// Handles GET request to {prefix}/users
func (ua *UsersAPI) getMeH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	usrTkn, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	user, errx := logic.GetMe(ctx, usrTkn, ua.ur, ua.sr)
	if errx != nil {
		logic.HTTPHandleError(ua.logger, w, r, errx)
		return
	}
	resp := GetMeRespBody{
		ID:        user.ID.String(),
		Username:  user.Username,
		Login:     user.Login,
		Name:      user.Name,
		CreatedAt: user.CreatedAt.String(),
	}
	respJson, err := json.Marshal(resp)
	if err != nil {
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(respJson)
}

// Handles GET request to /users/{userID}
// userID is passed into parameter as a reqUID (requested User ID)
func (uapi *UsersAPI) getUserH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	token, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	uidOrUname := chi.URLParam(r, "userID")

	opts := dto.GetUserOpts{
		UIDorUsername: uidOrUname,
		Token:         token,
	}
	_, userMap, errx := logic.GetUser(ctx, opts, uapi.ur, uapi.sr)
	if errx != nil {
		logic.HTTPHandleError(uapi.logger, w, r, errx)
		return
	}

	respJson, err := json.Marshal(userMap)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to marshal the response body"))
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(respJson)
}
