package logpkg

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"golang.org/x/exp/slog"
)

type RequestToSlogCfg struct {
	LogURL     bool
	LogHeaders bool
	LogBody    bool
}

func getFullURL(r *http.Request) string {
	// Get the scheme/protocol
	protocol := "http"
	if r.TLS != nil {
		protocol = "https"
	}

	return fmt.Sprintf("%s://%s%s%s%s",
		protocol,
		r.Host,
		r.URL.Path,
		r.URL.RawQuery,
		r.URL.Fragment)
}

func RequestToSlog(r *http.Request, cfg RequestToSlogCfg) []any {
	var attrs []any

	if cfg.LogURL {
		attrs = append(attrs, slog.String("method", r.Method))
		attrs = append(attrs, slog.String("url", getFullURL(r)))
	}

	if cfg.LogHeaders {
		var headersGroup []any
		for key, val := range r.Header {
			headersGroup = append(headersGroup,
				slog.String(key, strings.Join(val, ", ")))
		}

		attrs = append(attrs, slog.Group("headers", headersGroup...))
	}

	if cfg.LogBody {
		body, _ := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		attrs = append(attrs, slog.String("body", string(body)))
	}

	return attrs
}
