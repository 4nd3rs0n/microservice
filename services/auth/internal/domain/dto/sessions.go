package dto

import (
	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

type Session struct {
	SID       uuid.UUID `json:"sessionID"`
	UID       uuid.UUID `json:"usrID"`
	CreatedAt int64     `json:"createdAt"`
	DeviceInf string    `json:"deviceInf"`
	CreatorIP string    `json:"creatorIP"`
}

type KillSessionOpts struct {
	Token string
	SID   uuid.UUID
}

type GetAllMySessionsResp struct {
	Sessions        []entities.Session
	CountOfSessions int
}

type GetAllSessionsOpts struct {
	Token string
	UID   uuid.UUID
}
type GetAllSessionsResp struct {
	Sessions        []entities.Session
	CountOfSessions int
}

type GetSessionOpts struct {
	Token    string
	SidOrTkn string
}
type GetSessionResp struct {
	SID       uuid.UUID `json:"sessionID"`
	UID       uuid.UUID `json:"usrID"`
	CreatedAt int64     `json:"createdAt"`
	DeviceInf string    `json:"deviceInf"`
	CreatorIP string    `json:"creatorIP"`
	Current   bool      `json:"current"`
}
