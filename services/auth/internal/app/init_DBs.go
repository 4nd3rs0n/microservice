package app

import "context"

func (a *App) initDBs(
	ctx context.Context,
	pgURL string, pgMigrationsPath string,
	redisURL string, redisPassword string, redisDB int) error {

	var err error
	err = a.initPostgres(ctx, pgURL, pgMigrationsPath)
	err = a.initRedis(ctx, redisURL, redisPassword, redisDB)

	return err
}
