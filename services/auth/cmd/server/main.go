package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/app"
)

// Config

var certPath = os.Getenv("CERT_PATH")

// TODO: CLI args, config, environment variables

func main() {
	ctx := context.Background()
	cfg := app.Config{
		StartHTTP: true,
		HTTPport:  8000,

		StartGRPC: false,
		GRPCport:  50051,

		PgURL: fmt.Sprintf(
			"postgresql://%s:%s@%s:%s/%s",
			os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD"),
			os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"),
			os.Getenv("POSTGRES_DB")),
		MigrationPath: "sql",

		RedisURL: fmt.Sprintf("%s:%s",
			os.Getenv("REDIS_HOST"),
			os.Getenv("REDIS_PORT")),
		RedisPassword: os.Getenv("REDIS_PASSWORD"),
		RedisBD:       0,

		Stage: os.Getenv("STAGE"),
	}

	appInstance, err := app.InitApp(ctx, cfg)
	if err != nil {
		return
	}
	defer appInstance.Close(ctx)
	appInstance.Run(ctx)
}
