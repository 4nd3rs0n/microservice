package logic

import (
	"context"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

func Login(ctx context.Context, opts dto.LoginOpts,
	ur interfaces.UsersRepo, sr interfaces.SessionsRepo) (dto.LoginResponse, BLError) {

	var resp dto.LoginResponse

	uid, valid, err := ur.VerifyLoginCreds(ctx, entities.Creds{
		Password: opts.Password,
		Login:    opts.Login,
	})
	if err != nil {
		return resp, NewRepositoryErr(err.Error(),
			"Failed to validate your credentials due the unexpected error",
			map[string]any{})
	}
	if !valid {
		return resp, NewErr(ErrLoginFailed, ErrOpts{
			Log: false,
			States: map[string]any{
				"in": ERR_IN_PERMISSION_CHECK},
			HTTPCode: http.StatusUnauthorized,
		})
	}

	// TODO: Handle IP address as well
	token, err := sr.NewSession(ctx, uid, opts.DeviceInfo)
	if err != nil {
		return resp, NewRepositoryErr(err.Error(),
			"Failed to create session due the unexpected error",
			map[string]any{"uid": uid.String()})
	}
	resp.Token = token
	return resp, nil
}

func Logout(ctx context.Context, token string, sr interfaces.SessionsRepo) BLError {
	if len(token) != entities.TOKEN_LENGTH {
		return NewValidationErr(ErrTokenLength, map[string]any{})
	}

	sID, blerr := getSessionIdByToken(ctx, token, sr)
	if blerr != nil {
		return blerr
	}
	err := sr.KillSession(ctx, sID)
	if err != nil {
		return NewRepositoryErr(err.Error(),
			"Failed to kill your current session due the unexpected error",
			map[string]any{})
	}

	return nil
}
