CREATE TABLE IF NOT EXISTS groups (
  id  UUID PRIMARY KEY DEFAULT gen_random_uuid() NOT NULL,
  name VARCHAR(100),
  description VARCHAR(500)
);

CREATE TABLE IF NOT EXISTS invites (
  -- gid and inviteId should be uniq together
  gid UUID REFERENCES groups(id) ON DELETE CASCADE NOT NULL,
  inviteId INT NOT NULL,
  name VARCHAR(64) NOT NULL, 
  -- There is will be stored not token itself, but the token hash
  token BYTEA UNIQUE NOT NULL, 
  -- Password iss also gotta be hashed
  -- If password doesn't required just keep it null
  passwd BYTEA,
);

CREATE TABLE IF NOT EXISTS threads (
  id UUID PRIMARY KEY DEFAULT gen_random_uuid() NOT NULL,
  name VARCHAR(64) NOT NULL,
);

CREATE TABLE IF NOT EXISTS linked_threads (
  gid UUID REFERENCES groups(id) ON DELETE CASCADE NOT NULL,
  tid UUID REFERENCES threads(id) ON DELETE CASCADE NOT NULL,
  CONSTRAINT gid_tid_unique UNIQUE (gid, tid)
);

CREATE TABLE IF NOT EXISTS group_members (
  -- User IDs are taken from the auth service.
  uid UUID NOT NULL,
  gid UUID REFERENCES groups(id) ON DELETE CASCADE NOT NULL,
  CONSTRAINT uid_gid_unique UNIQUE (uid, gid)
);

CREATE TABLE IF NOT EXISTS roles (
  gid UUID REFERENCES groups(id) ON DELETE CASCADE NOT NULL,
  roleId INT NOT NULL,
  name VARCHAR(64) NOT NULL,
  -- TODO: Premission system
  premissions VARCHAR
);

CREATE TABLE IF NOT EXISTS linked_roles (
  rid U REFERENCES roles(id) ON DELETE CASCADE NOT NULL
  mid
);
