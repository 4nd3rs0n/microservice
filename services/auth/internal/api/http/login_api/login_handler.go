package login

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/logic"
)

type LoginReqBody struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}
type LoginRespBody struct {
	Token string `json:"token"`
}

func (la *LoginAPI) loginH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	var body LoginReqBody
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Failed to parse body"))
		return
	}

	opts := dto.LoginOpts{
		Login:    body.Login,
		Password: body.Password,
		// TODO: DeviceInfo
	}
	fresp, errx := logic.Login(ctx, opts, la.ur, la.sr)
	if errx != nil {
		logic.HTTPHandleError(la.logger, w, r, errx)
		return
	}

	resp := LoginRespBody{
		Token: fresp.Token,
	}
	respJson, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to marshal the response body"))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(respJson)
}
