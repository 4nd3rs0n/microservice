package sessions_api

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

// TODO: rewrite using domain layer
func (sa *SessionsAPI) GetAllSessions(ctx context.Context,
	w http.ResponseWriter, r *http.Request, usrTkn string) (err error) {
	uID, err := sa.sr.GetUserIdByToken(ctx, usrTkn)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		return
	}

	usrSessions, err := sa.sr.GetAllSessions(ctx, uID)
	if err != nil {
		return
	}
	resp, err := json.Marshal(usrSessions)
	if err != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(resp)
	return
}
