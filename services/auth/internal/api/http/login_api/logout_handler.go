package login

import (
	"context"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/logic"
)

// Handles GET request to {prefix}/logout route
func (la *LoginAPI) logoutH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	token, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	errx := logic.Logout(ctx, token, la.sr)
	if errx != nil {
		logic.HTTPHandleError(la.logger, w, r, errx)
		return
	}
	w.WriteHeader(http.StatusOK)
}
