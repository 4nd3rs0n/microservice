package users_api

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/logic"
)

func (uapi *UsersAPI) deleteMeH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	token, next, err := middleware.TokenPrehandler(w, r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !next {
		return
	}

	errx := logic.RemoveMe(ctx, token, uapi.ur, uapi.sr)
	if errx != nil {
		logic.HTTPHandleError(uapi.logger, w, r, errx)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// TODO
func (ua *UsersAPI) deleteUserH(ctx context.Context,
	w http.ResponseWriter, r *http.Request, reqUID uuid.UUID) {
}
