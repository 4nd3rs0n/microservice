package username

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const UsernameMinLen = 3
const UsernameMaxLen = 40

var (
	ErrInvalidUsername = errors.New("Invalid username")
	ErrUnexpected      = errors.New("unknown error")
)

var UsernameBaseRegexp = fmt.Sprintf(
	`^(?:@)?[a-zA-Z0-9]{%d,%d}$`,
	UsernameMinLen, UsernameMaxLen)
var UsernameRegexp = fmt.Sprintf(`%s:[0-9]+$`, UsernameBaseRegexp)

func Parse(user string) (uname Username, err error) {
	if matched, err := regexp.MatchString(UsernameRegexp, user); !matched {
		if err != nil {
			return nil, err
		}
		return nil, ErrInvalidUsername
	}

	if user[0] == '@' {
		user = user[1:]
	}

	unameSlice := strings.Split(user, spliter)
	if len(unameSlice) < 2 {
		return nil, ErrUnexpected
	}

	unameBase := unameSlice[0]
	unameID, err := strconv.Atoi(unameSlice[1])
	if err != nil {
		return nil, err
	}

	username, err := NewUsername(unameBase, unameID)
	return username, err
}
