package adminapi

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
)

type AdminAPI struct {
}

func NewAdminAPI() AdminAPI {
	return AdminAPI{}
}

func (aapi *AdminAPI) API(ctx context.Context, r *chi.Mux) {
	r.Route("/admins", func(r chi.Router) {
		r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			switch r.Method {
			case http.MethodPost:
				// Make user an admin
			case http.MethodGet:

			}
		})
	})
}
