package logic

import (
	"context"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
)

func ChangeDisplayedName(ctx context.Context, opts dto.ChangeNameOpts,
	ur interfaces.UsersRepo, sr interfaces.SessionsRepo) BLError {

	if err := opts.Validate(); err != nil {
		return NewValidationErr(err.Error(), map[string]any{})
	}
	uid, blerr := getUserIdByToken(ctx, opts.Token, sr)
	if blerr != nil {
		return blerr
	}
	err := ur.ChangeDisplayedName(ctx, uid, opts.NewName)
	if err != nil {
		return NewRepositoryErr(err.Error(),
			"Failed to change name due the unexpected error",
			map[string]any{"uid": uid, "name": opts.NewName})
	}
	return nil
}

func ChangePassword(ctx context.Context, opts dto.ChangePasswordOpts,
	ur interfaces.UsersRepo, sr interfaces.SessionsRepo) BLError {

	if err := opts.Validate(); err != nil {
		return NewValidationErr(err.Error(), map[string]any{})
	}
	uid, blerr := getUserIdByToken(ctx, opts.Token, sr)
	if blerr != nil {
		return blerr
	}

	match, err := ur.VerifyIdAndPassword(ctx, uid, opts.OldPassord)
	if err != nil {
		return NewRepositoryErr(err.Error(),
			"Failed to verify your old credentials due the unexpected error",
			map[string]any{"uid": uid})
	}
	if !match {
		return NewErr(ErrPermissionDenied, ErrOpts{
			Log:      false,
			States:   map[string]any{"uid": uid},
			HTTPResp: "Old password doesn't match. Permission denied",
			HTTPCode: http.StatusForbidden})
	}

	err = ur.ChangePassword(ctx, uid, opts.NewPassword)
	if err != nil {
		return NewRepositoryErr(err.Error(),
			"Failed to change password due the unexpected error.",
			map[string]any{})
	}
	return nil
}

func ChangeLogin(ctx context.Context, opts dto.ChangeLoginOpts,
	ur interfaces.UsersRepo, sr interfaces.SessionsRepo) BLError {

	if err := opts.Validate(); err != nil {
		return NewValidationErr(err.Error(), map[string]any{})
	}
	uid, blerr := getUserIdByToken(ctx, opts.Token, sr)
	if blerr != nil {
		return blerr
	}

	match, err := ur.VerifyIdAndPassword(ctx, uid, opts.Password)
	if err != nil {
		return NewRepositoryErr(err.Error(),
			"Failed to verify your old credentials due the unexpected error",
			map[string]any{"uid": uid})
	}
	if !match {
		return NewErr(ErrPermissionDenied, ErrOpts{
			Log:      false,
			States:   map[string]any{"uid": uid},
			HTTPResp: "Old password doesn't match. Permission denied",
			HTTPCode: http.StatusForbidden})
	}

	err = ur.ChangeLogin(ctx, uid, opts.NewLogin)
	if err != nil {
		return NewRepositoryErr(err.Error(),
			"Failed to change login due the unexpected error.",
			map[string]any{})
	}
	return nil
}
