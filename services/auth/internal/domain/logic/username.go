package logic

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/username"
)

func GetFreeUsernameId(
	ctx context.Context,
	opts dto.TestUsernameOpts,
	ur interfaces.UsersRepo) (int, BLError) {

	if err := opts.Validate(); err != nil {
		return 0, NewValidationErr(err.Error(), map[string]any{})
	}
	id, err := ur.GetFreeUsernameId(ctx, opts.UsernameBase)
	if err != nil {
		return 0, NewRepositoryErr(err.Error(),
			"Failed to get free ID for this Username due the unexpected error",
			map[string]any{
				"in":       ERR_IN_REPOSITORY,
				"username": opts.UsernameBase})
	}
	return id, nil
}

func SetUsername(
	ctx context.Context,
	opts dto.SetUsernameOpts,
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo) BLError {

	if err := opts.Validate(); err != nil {
		return NewValidationErr(err.Error(), map[string]any{})
	}

	uid, blerr := getSessionIdByToken(ctx, opts.Token, sr)
	if blerr != nil {
		return blerr
	}

	var err error
	if opts.UsernameBase == "" {
		err = ur.SetNilUsername(ctx, uid)
	} else {
		err = ur.SetUsername(ctx, uid, opts.UsernameBase)
	}

	if err != nil {
		return NewRepositoryErr(err.Error(),
			"Failed to set Username due the unexpected error",
			map[string]any{
				"in":       ERR_IN_REPOSITORY,
				"uid":      uid.String(),
				"username": opts.UsernameBase})
	}
	return nil
}

func GetUIdByUsername(
	ctx context.Context,
	opts dto.GetUIdByUsernameOpts,
	ur interfaces.UsersRepo) (uuid.UUID, BLError) {

	uid, err := ur.GetIdByUsername(ctx, opts.Username)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return uuid.Nil, NewErr(err.Error(), ErrOpts{
				Log:      false,
				HTTPResp: "User with such username wasn't found",
				HTTPCode: http.StatusBadRequest,
			})
		}
		return uuid.Nil, NewRepositoryErr(err.Error(),
			"Failed to get User ID due the unexpected error",
			map[string]any{})
	}
	return uid, nil
}

// If string is uuid just parse and returns it
// Else trying to get User ID by the username
func uuidOrUsername(
	ctx context.Context,
	uidOrUname string,
	ur interfaces.UsersRepo) (uuid.UUID, BLError) {

	uid, err := uuid.Parse(uidOrUname)
	if err == nil {
		return uid, nil
	}

	uname, err := username.Parse(uidOrUname)
	if err != nil {
		return uuid.Nil, NewErr(err.Error(), ErrOpts{
			Log:      false,
			HTTPResp: "Wrong Username or User ID. Failed to parse",
			HTTPCode: http.StatusBadRequest})
	}
	uid, err = GetUIdByUsername(ctx, dto.GetUIdByUsernameOpts{
		Username: uname,
	}, ur)
	if err != nil {
		if err == entities.ErrEntityNotFound {
			return uuid.Nil, NewErr(ErrEntityNotFound, ErrOpts{
				Log:      false,
				States:   map[string]any{"in": ERR_IN_REPOSITORY, "username": uname},
				HTTPResp: "Requested user wasn't found",
				HTTPCode: http.StatusNotFound,
			})
		}
		return uuid.Nil, NewRepositoryErr(err.Error(),
			"Failed to get User ID by username due the unexpected error",
			map[string]any{"username": uname})
	}

	return uid, nil
}
