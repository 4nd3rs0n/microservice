package login

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/interfaces"
	"golang.org/x/exp/slog"
)

type LoginAPI struct {
	ur     interfaces.UsersRepo
	sr     interfaces.SessionsRepo
	logger *slog.Logger
}

func NewLoginAPI(
	ur interfaces.UsersRepo,
	sr interfaces.SessionsRepo,
	logger *slog.Logger) LoginAPI {
	return LoginAPI{
		ur:     ur,
		sr:     sr,
		logger: logger,
	}
}

func (la *LoginAPI) API(ctx context.Context, r *chi.Mux) {
	r.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		// Middlewares
		middleware.EnableCORS(&w)

		switch r.Method {
		case http.MethodOptions:
			w.WriteHeader(http.StatusOK)
		case http.MethodPost:
			la.loginH(ctx, w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})

	r.HandleFunc("/logout", func(w http.ResponseWriter, r *http.Request) {
		// Middlewares
		middleware.EnableCORS(&w)

		switch r.Method {
		case http.MethodOptions:
			w.WriteHeader(http.StatusOK)
		case http.MethodGet:
			la.logoutH(ctx, w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}
