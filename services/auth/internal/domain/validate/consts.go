package validate

const MIN_NAME_LEN = 1
const MAX_NAME_LEN = 40

const MIN_LOGIN_LEN = 3
const MAX_LOGIN_LEN = 40

const MIN_PASSWORD_LEN = 6

const MIN_DEVICE_INF_LEN = 150
const MAX_DEVICE_INF_LEN = 150
