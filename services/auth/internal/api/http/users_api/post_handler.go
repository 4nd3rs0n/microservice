package users_api

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/logic"
)

type CreateUserReqBody struct {
	Name     string `json:"name"`
	Login    string `json:"login"`
	Password string `json:"password"`
}
type CreateUserRespBody struct {
	Token string `json:"token"`
}

// Handles POST request to the /user route
func (uapi *UsersAPI) postUserH(ctx context.Context,
	w http.ResponseWriter, r *http.Request) {

	var body CreateUserReqBody
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	opts := dto.CreateUserOpts{
		Name:     body.Name,
		Login:    body.Login,
		Password: body.Password,
	}

	fresp, errx := logic.CreateUser(ctx, opts, uapi.ur, uapi.sr)
	if errx != nil {
		logic.HTTPHandleError(uapi.logger, w, r, errx)
		return
	}

	resp := CreateUserRespBody{
		Token: fresp.Token,
	}
	respJson, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(respJson)
	return
}
