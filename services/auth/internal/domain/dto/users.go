package dto

import (
	"fmt"
	"time"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/validate"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/username"
)

// Options
type CreateUserOpts struct {
	// User info
	Name     string
	Login    string
	Password string
	// Session info
	DeviceInfo string
	IpAddr     string
}

func (opts *CreateUserOpts) Validate() error {
	if err := validate.ValidateName(opts.Name); err != nil {
		return fmt.Errorf("Name is invalid: %s", err.Error())
	}
	if err := validate.ValidateLogin(opts.Login); err != nil {
		return fmt.Errorf("Login is invalid: %s", err.Error())
	}
	if err := validate.ValidateDeviceInfo(opts.DeviceInfo); err != nil {
		return fmt.Errorf("Device info is invalid: %s", err.Error())
	}
	// TODO: Implement IP addresses later
	// if err := validate.ValidateIP(opts.IpAddr); err != nil {
	// 	return fmt.Errorf("IP address is invalid: %s", err.Error())
	// }
	return nil
}

type CreateUserResp struct {
	Token string
}

type RemoveUserOpts struct {
	UIDorUsername string
	Token         string `validate:"len=64"`
}

func (opts *RemoveUserOpts) Validate() error {
	if err := validate.ValidateSessionToken(opts.Token); err != nil {
		return fmt.Errorf("Session token is invalid: %s", err.Error())
	}
	return nil
}

type GetUserOpts struct {
	UIDorUsername string
	Token         string `validate:"len=64"`
}

func (opts *GetUserOpts) Validate() error {
	if err := validate.ValidateSessionToken(opts.Token); err != nil {
		return fmt.Errorf("Session token is invalid: %s", err.Error())
	}
	return nil
}

// Some fields may be nil or an empty string
type GetUserResp struct {
	ID        uuid.UUID
	Name      string
	Username  username.Username
	Banned    bool
	CreatedAt time.Time
}

type GetMeResp struct {
	ID        uuid.UUID
	Username  string
	Login     string
	Name      string
	CreatedAt time.Time
}

// TODO
type ChangeNameOpts struct {
	Token   string
	NewName string
}

func (opts *ChangeNameOpts) Validate() error {
	if err := validate.ValidateSessionToken(opts.Token); err != nil {
		return err
	}
	if err := validate.ValidateName(opts.NewName); err != nil {
		return err
	}
	return nil
}

type ChangePasswordOpts struct {
	Token       string
	OldPassord  string
	NewPassword string
}

func (opts *ChangePasswordOpts) Validate() error {
	if err := validate.ValidateSessionToken(opts.Token); err != nil {
		return err
	}
	if err := validate.ValidatePassword(opts.OldPassord); err != nil {
		return err
	}
	if err := validate.ValidatePassword(opts.NewPassword); err != nil {
		return err
	}
	return nil
}

type ChangeLoginOpts struct {
	Token    string
	Password string
	NewLogin string
}

func (opts *ChangeLoginOpts) Validate() error {
	if err := validate.ValidateSessionToken(opts.Token); err != nil {
		return err
	}
	if err := validate.ValidatePassword(opts.Password); err != nil {
		return err
	}
	if err := validate.ValidateLogin(opts.NewLogin); err != nil {
		return err
	}
	return nil
}

type TestUsernameOpts struct {
	UsernameBase string
}

func (opts *TestUsernameOpts) Validate() error {
	return validate.ValidateUsernameBase(opts.UsernameBase)
}

type SetUsernameOpts struct {
	Token        string
	UsernameBase string
}

func (opts *SetUsernameOpts) Validate() error {
	if err := validate.ValidateSessionToken(opts.Token); err != nil {
		return err
	}
	if err := validate.ValidateUsernameBase(opts.UsernameBase); err != nil &&
		opts.UsernameBase != "" {

		return err
	}
	return nil
}

type GetUIdByUsernameOpts struct {
	Username username.Username
}
